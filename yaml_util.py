#!/usr/bin/env python
'''
yaml_util.py - read icon descriptions from YAML file

Copyright (C) 2015, ~suv <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
'''
# pylint: disable=invalid-name
# pylint: disable=missing-docstring

# system modules
import os
import yaml
# internal modules
import inkex

# ------------------------------------------------------------------- #
# globals
# ------------------------------------------------------------------- #


# ------------------------------------------------------------------- #
# utility functions for extracting icon mapping from YAML files
# ------------------------------------------------------------------- #

def parse_icons_yml(config_file):
    '''
    parameters: config file (full path)
    parses config file and creates dict using unicode as key, id and name as value
    returns: dict

    Metadata for icon font projects using Jekyll as build system

    Icon fonts known to provide this format:
    * Font-Awesome
    * Elusive Icons

    '''
    doc = None
    adict = {}
    try:
        with open(config_file, 'r') as f:
            doc = yaml.load(f)
    except Exception:
        pass
    if doc is not None:
        for ele in doc['icons']:
            adict[ele['unicode']] = {'id': ele['id'], 'name': ele['name']}
        inkex.debug(adict)
        return adict


def parse_config_yml(config_file):
    '''
    parameters: config file (full path)
    parses config file and creates dict using unicode as key, uid, css as value
    returns: dict

    Metadata for icon fonts created with font-builder (fontello)

    Icon fonts known to provide this format:
    * fontelico.font

    '''
    doc = None
    adict = {}
    try:
        with open(config_file, 'r') as f:
            doc = yaml.load(f)
    except Exception:
        pass
    if doc is not None:
        for ele in doc['glyphs']:
            if type(ele['code']) is str:
                adict[ele['code'][2:]] = {'id': ele['uid'], 'name': ele['css']}
            else:
                adict[hex(ele['code'])[2:]] = {'id': ele['uid'], 'name': ele['css']}
        inkex.debug(adict)
        return adict


def parse_config_file(config_file):
    '''
    parameters: config file (full path)
    dispatcher for known formats based on file name (needs fixing)
    returns: dict
    '''
    if os.path.isfile(config_file):
        filename = os.path.split(config_file)[1]
        if filename == "icons.yml":
            return parse_icons_yml(config_file)
        elif filename == "config.yml":
            return parse_config_yml(config_file)


def get_icon_info_by_unicode(unicode_str, config_file):
    '''
    parameters: hex unicode as string, config file
    parses config file, searches in created dict for value of unicode string as key
    returns: dict
    '''
    try:
        return parse_config_file(config_file)[unicode_str[2:]]
    except KeyError:
        pass


# vim: expandtab shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=99
