#!/usr/bin/env python
'''
json_util.py - read icon descriptions from json file

Copyright (C) 2015, ~suv <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
'''
# pylint: disable=invalid-name
# pylint: disable=missing-docstring

# system modules
import os
import json
# internal modules
import inkex

# ------------------------------------------------------------------- #
# globals
# ------------------------------------------------------------------- #


# ------------------------------------------------------------------- #
# utility functions for extracting icon mapping from JSON files
# ------------------------------------------------------------------- #

def parse_manifest_json(config_file):
    '''
    parameters: config file (full path)
    parses config file and creates dict using unicode as key, id and name as value
    returns: dict

    Icon fonts known to provide this format:
    * Ionicons

    '''
    doc = None
    adict = {}
    try:
        with open(config_file, 'r') as f:
            doc = json.load(f)
    except Exception:
        pass
    if doc is not None:
        for ele in doc['icons']:
            adict[str(ele['code'])[2:]] = {'id': str(ele['code'])[2:], 'name': str(ele['name'])}
        inkex.debug(adict)
        return adict


def parse_config_file(config_file):
    '''
    parameters: config file (full path)
    dispatcher for known formats based on file name (needs fixing)
    returns: dict
    '''
    if os.path.isfile(config_file):
        filename = os.path.split(config_file)[1]
        if filename == "manifest.json":
            return parse_manifest_json(config_file)


# vim: expandtab shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=99
