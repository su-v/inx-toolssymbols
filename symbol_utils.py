#!/usr/bin/env python
'''
symbol_utils.py - conversion utilities for icon and symbol sets

Copyright (C) 2015-2016, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
'''
# pylint: disable=invalid-name
# pylint: disable=missing-docstring
# pylint: disable=too-many-lines
# pylint: disable=too-many-statements
# pylint: enable=fixme

# system modules
import os
import sys
import re
import locale
from unidecode import unidecode
from copy import deepcopy
from StringIO import StringIO
import csv
# internal modules
import inkex
import simplepath
import simpletransform


# ------------------------------------------------------------------- #
# globals
# ------------------------------------------------------------------- #

DEBUG = False

# NSS = {
#     u'sodipodi' :u'http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd',
#     u'cc'       :u'http://creativecommons.org/ns#',
#     u'ccOLD'    :u'http://web.resource.org/cc/',
#     u'svg'      :u'http://www.w3.org/2000/svg',
#     u'dc'       :u'http://purl.org/dc/elements/1.1/',
#     u'rdf'      :u'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
#     u'inkscape' :u'http://www.inkscape.org/namespaces/inkscape',
#     u'xlink'    :u'http://www.w3.org/1999/xlink',
#     u'xml'      :u'http://www.w3.org/XML/1998/namespace'
# }

SVG_NAMESPACE = inkex.NSS['svg']
SVG = "{%s}" % SVG_NAMESPACE

NS_DEFAULT = {None: SVG_NAMESPACE}

NS_MAP = dict(inkex.NSS)
NS_MAP.pop('svg', None)
NS_MAP.pop('ccOLD', None)
NS_MAP.pop('xml', None)
NS_MAP.update(NS_DEFAULT)

UUCONV = inkex.Effect._Effect__uuconv

BASIC_SVG_OBJECTS = ['rect', 'circle', 'ellipse', 'line', 'polyline', 'polygon',
                     'path', 'g', 'switch']

IMPORT_TAGS = [inkex.addNS(i, 'svg') for i in BASIC_SVG_OBJECTS]

LABEL_MODE = "tspan"  # alternative: "tref"

LABEL_ID_SUFFIX = "_text"

SYMBOL_ID_SUFFIX = "_symbol"


# ------------------------------------------------------------------- #
# utility functions
# ------------------------------------------------------------------- #

def set_or_del_key_val(node, key, val, default=0):
    '''
    parameters: node, key, val, default (optional)
    sets key of node to val or deletes key if val is default (0)
    returns: ?
    '''
    if val != default:
        node.set(key, str(val))
    elif key in node.attrib:
        del node.attrib[key]


def set_or_del_key_str(node, key, string, unset=""):
    '''
    parameters: node, key, string, unset (optional)
    sets key of node to string or deletes key if string is unset ("")
    returns: ?
    '''
    if string != unset:
        node.set(key, string)
    elif key in node.attrib:
        del node.attrib[key]


# ------------------------------------------------------------------- #
# utility functions: convert group <-> symbol
# ------------------------------------------------------------------- #

def copy_node_to_node(source, target, move=True):
    '''
    parameters: source node, target node
    copies children of source node into target node
    copies id to target node and removes id from source node
    returns: target node
    '''
    if len(source):
        for item in source.iterchildren():
            if move:
                target.append(item)
            else:  # copy
                target.append(deepcopy(item))
    else:
        if move:
            target.append(source)
        else:
            target.append(deepcopy(source))
    target.attrib['id'] = source.attrib['id']
    if move:
        del source.attrib['id']
    return target


# ------------------------------------------------------------------- #
# utility functions for converting group <-> symbol
# ------------------------------------------------------------------- #

def is_group(node):
    '''
    parameters: group element
    checks whether node is a <g> element
    returns: true|false
    '''
    return node.tag == inkex.addNS('g', 'svg')


def is_use(node):
    '''
    parameters: node
    checks whether node is a <use> element
    returns: true|false
    '''
    return node.tag == inkex.addNS('use', 'svg')


def is_symbol(node):
    '''
    parameters: node
    checks whether node is a <symbol> definition
    returns: true|false
    '''
    return node.tag == inkex.addNS('symbol', 'svg')


def add_layer(parent, label="Layer"):
    '''
    parameters: parent node, label (optional)
    adds layer to parent
    returns: new layer
    '''
    layergroup = inkex.etree.SubElement(parent, inkex.addNS('g', 'svg'))
    layergroup.set(inkex.addNS('label', 'inkscape'), label)
    layergroup.set(inkex.addNS('groupmode', 'inkscape'), 'layer')
    return layergroup


def add_group(parent):
    '''
    parameters: parent node
    adds <g> element to parent
    returns: new group
    '''
    return inkex.etree.SubElement(parent, inkex.addNS('g', 'svg'))


def add_symbol(parent):
    '''
    parameters: parent node
    adds <symbol> element to parent
    returns: new symbol
    '''
    new_symbol = inkex.etree.SubElement(parent, inkex.addNS('symbol', 'svg'))
    new_symbol.attrib['style'] = "overflow:visible"
    return new_symbol


def add_instance(node, parent):
    '''
    parameter: symbol definition, target layer (parent node)
    creates new instance of symbol on layer
    returns: clone
    '''
    use_def = inkex.etree.SubElement(parent, inkex.addNS('use', 'svg'))
    use_def.attrib[inkex.addNS('href', 'xlink')] = '#{}'.format(node.attrib['id'])
    set_id(use_def, 'use_{0}'.format(get_id(node)))
    return use_def


def del_instance(instance, symbol):
    '''
    parameters: clone (symbol instance), symbol definition
    deletes use object if instance of symbol
    returns: ?
    '''
    # TODO: implement del_instance()
    msg = 'TODO: delete {0} if instance of {1}'.format(instance.attrib['id'], symbol.attrib['id'])
    inkex.debug(msg)


def is_instance_of(use, symbol):
    '''
    paramters: clone, symbol definition
    checks whether clone is instance of symbol
    returns: true|false
    '''
    # TODO: implement is_instance_of()
    msg = 'TODO: check if {0} is instance of {1}'.format(use.attrib['id'], symbol.attrib['id'])
    inkex.debug(msg)


def get_instances(node, parent):
    '''
    parameters: symbol definition, source layer (parent node)
    creates a list of all <use> elements on layer referencing symbol
    returns: list of clones
    '''
    # TODO: implement get_instances()
    msg = 'TODO: list {0} instances in layer {1}'.format(node.attrib['id'], parent.attrib['id'])
    inkex.debug(msg)


def get_symbol_from_instance(node, defs):
    '''
    parameters: node, defs
    checks whether node is a <use> element referencing a <symbol> element in defs
    returns: node of symbol definition or None
    '''
    if is_use(node):
        href_id = node.get(inkex.addNS('href', 'xlink'))[1:]
        path = './/svg:symbol[@id="{0}"]'.format(href_id)
        try:
            return defs.xpath(path, namespaces=inkex.NSS)[0]
        except IndexError:
            return None


def get_symbols(parent):
    '''
    parameters: parent node (defs)
    creates list of all <symbol> elements in <defs>
    returns: list of symbols
    '''
    glist = []
    for item in parent.iterchildren():
        if is_symbol(item):
            glist.append(item)
        elif is_group(item):
            glist = glist + get_symbols(item)
    return glist


def get_groups(parent):
    '''
    parameters: layer (parent node)
    creates a list of all groups in node
    returns: list of elements
    '''
    glist = []
    for item in parent.iterchildren():
        if is_group(item):
            glist.append(item)
    return glist


def get_uses(parent):
    '''
    parameters: layer (parent node)
    creates a list of all groups in node
    returns: list of elements
    '''
    glist = []
    for item in parent.iterchildren():
        if is_use(item):
            glist.append(item)
    return glist


def get_objects(type_, parent):
    '''
    parameters: object type, source layer (parent node)
    creates a list of all objects of type type in layer
    returns: list of elements
    '''
    path = './/svg:{0}'.format(type_)
    return parent.xpath(path, namespaces=inkex.NSS)


# ------------------------------------------------------------------- #
# utility functions for copying labels (id, label, title)
# ------------------------------------------------------------------- #

def get_id(node):
    '''
    paramaters: node
    gets value for 'id' attribute
    returns: value (string)
    '''
    if 'id' in node.attrib:
        return node.attrib['id']


def set_id(node, val):
    '''
    paramaters: node, attribute value
    sets value for 'id' attribute
    returns: ?
    '''
    if val:
        node.attrib['id'] = val


def del_id(node):
    '''
    paramaters: node
    deletes attribute 'id'
    returns: ?
    '''
    if 'id' in node.attrib:
        del node.attrib['id']


def get_label(node):
    '''
    paramaters: node
    gets value for inkscape:label attribute
    returns: value (string)
    '''
    try:
        return node.attrib[inkex.addNS('label', 'inkscape')]
    except KeyError:
        pass


def set_label(node, val):
    '''
    paramaters: node, attribute value
    sets value for inkscape:label attribute
    returns: ?
    '''
    if val:
        node.attrib[inkex.addNS('label', 'inkscape')] = val


def del_label(node):
    '''
    paramaters: node
    deletes attribute 'inkscape:label'
    returns: ?
    '''
    try:
        del node.attrib[inkex.addNS('label', 'inkscape')]
    except KeyError:
        pass


def is_title(node):
    '''
    parameters: node
    checks whether node is a <title> element
    returns: true|false
    '''
    return node.tag == inkex.addNS('title', 'svg')


def get_title(node):
    '''
    parameters: node
    checks for title child element of node
    returns: title element
    '''
    return node.find(inkex.addNS('title', 'svg'))


def set_title(node, val):
    '''
    parameters: node
    checks for title child element of node, adds one if missing, sets text to val
    returns: title element
    '''
    title = add_title(node)
    title.text = val
    return title


def add_title(node):
    '''
    parameters: node
    adds title child element to node if not present
    returns: title element
    '''
    title = get_title(node)
    if title is None:
        title = inkex.etree.Element(inkex.addNS('title', 'svg'))
        node.insert(0, title)
        set_id(title, 'title_{0}'.format(get_id(node)))
    return title


def del_title(node):
    '''
    parameters: node
    deletes child element 'title' from node
    returns: ?
    '''
    try:
        node.remove(get_title(node))
    except ValueError:
        pass
    except TypeError:
        pass


def get_title_text(node):
    '''
    parameters: node
    checks child elements for tag 'title', retrieves text if present
    returns: text content of child <title>
    '''
    try:
        return get_title(node).text
    except AttributeError:
        pass


def set_title_text(node, val):
    '''
    parameters: node, string for child content
    checks child elements for tag 'title', appends it if not present and sets text content
    returns: ?
    '''
    if val:
        add_title(node).text = val


# ------------------------------------------------------------------- #
# utility functions for on-canvas text labels
# ------------------------------------------------------------------- #

def add_text(parent):
    '''
    parameters: parent (current layer)
    Creates new text element
    returns: text element
    '''
    return inkex.etree.SubElement(parent, inkex.addNS('text', 'svg'))


def add_tspan(parent, content):
    '''
    parameters: parent (text), string
    Creates tspan text subelement with content of string
    returns: tspan
    '''
    tspan_def = inkex.etree.SubElement(parent, inkex.addNS('tspan', 'svg'))
    tspan_def.text = content
    return tspan_def


def add_tref(parent, node):
    '''
    parameters: parent (text), node
    Creates tref text subelement referencing node (for content)
    returns: tref
    '''
    tref_def = inkex.etree.SubElement(parent, inkex.addNS('tref', 'svg'))
    tref_def.attrib[inkex.addNS('href', 'xlink')] = '#{}'.format(node.attrib['id'])
    return tref_def


def get_id_ref(parent, node):
    '''
    parameters: parent (current layer), node
    Searches for <text> elements with ID based on node ID
    returns: first <text> found with ID based on node ID
    '''
    path = './/svg:tspan[@id="{}{}"]'.format(get_id(node), LABEL_ID_SUFFIX)
    result = parent.xpath(path, namespaces=inkex.NSS)
    if result:
        return result[0].getparent()
    else:
        return None


def get_tref(parent, node):
    '''
    parameters: parent (current layer), node
    Searches for <tref> elements linked to node
    returns: first <text> found containing tref referencing node
    '''
    path = './/svg:tref[@xlink:href="#{0}"]'.format(get_id(node))
    result = parent.xpath(path, namespaces=inkex.NSS)
    if result:
        return result[0].getparent()
    else:
        return None


def add_text_with_id_ref(parent, node):
    '''
    paramters: parent (current layer), referenced node
    Creates new text object in parent with text content of referenced node, ID based on ref node
    returns: text node
    '''
    new_text = add_text(parent)
    new_tspan = add_tspan(new_text, get_title_text(node.getparent()))
    set_id(new_tspan, '{}{}'.format(get_id(node), LABEL_ID_SUFFIX))
    return new_text


def add_text_with_tref(parent, node):
    '''
    parameters: parent (current layer), referenced node
    Creates new text object in parent with tref linked to node
    returns: text node
    '''
    new_text = add_text(parent)
    new_tref = add_tref(new_text, node)
    set_id(new_tref, '{}{}'.format(get_id(node), LABEL_ID_SUFFIX))
    return new_text


def get_text_with_id_ref(parent, node):
    '''
    parameters: parent (current layer), referenced node (<title> of group or symbol)
    Searches for text objects with id which is based on id of referenced node. creates text if None
    returns: text element
    '''
    id_ref_text = None
    if node is not None:
        id_ref_text = get_id_ref(parent, node)
        if id_ref_text is None:
            id_ref_text = add_text_with_id_ref(parent, node)
    return id_ref_text


def get_text_with_tref(parent, node):
    '''
    parameters: parent (current layer), referenced node
    Searches for text objects with child <tref> referencing node, adds text with tref if None
    returns: text element
    '''
    tref_text = None
    if node is not None:
        tref_text = get_tref(parent, node)
        if tref_text is None:
            tref_text = add_text_with_tref(parent, node)
    return tref_text


def del_text_with_id_ref(parent, node):
    '''
    parameters: parent (current layer), referenced node
    Searches in parent for <text> elements with ID based on ID of referenced node
    returns: ?
    '''
    if node is not None:
        id_ref_text = get_id_ref(parent, node)
        if id_ref_text is not None:
            id_ref_text.getparent().remove(id_ref_text)


def del_text_with_tref(parent, node):
    '''
    parameters: parent (current layer), referenced node
    Searches in parent for <tref> referencing node and deletes first instance found
    returns: ?
    '''
    if node is not None:
        tref_text = get_tref(parent, node)
        if tref_text is not None:
            tref_text.getparent().remove(tref_text)


# ------------------------------------------------------------------- #
# utility functions for styles
# ------------------------------------------------------------------- #

def get_style(node):
    '''
    paramaters: node
    gets value for 'style' attribute
    returns: value (string)
    '''
    if 'style' in node.attrib:
        return node.attrib['style']


def set_style(node, val):
    '''
    paramaters: node, attribute value
    sets value for 'style' attribute
    returns: ?
    '''
    if val:
        node.attrib['style'] = val


def add_style(node, val):
    '''
    parameters: node, val
    appends val to 'style' attribute if present, else sets attribute
    returns: ?
    '''
    if val:
        if 'style' in node.attrib:
            # TODO: parse style attribute, replace instead of append if property is present
            node.attrib['style'] = '{};{}'.format(node.attrib['style'], val)
        else:
            set_style(node, val)


def del_style(node):
    '''
    paramaters: node
    deletes attribute 'style'
    returns: ?
    '''
    if 'style' in node.attrib:
        del node.attrib['style']


# ------------------------------------------------------------------- #
# utility functions for transforms
# ------------------------------------------------------------------- #

def get_transform(node):
    '''
    paramaters: node
    gets value for 'transform' attribute
    returns: value (string)
    '''
    if 'transform' in node.attrib:
        return node.attrib['transform']


def set_transform(node, val):
    '''
    paramaters: node, attribute value
    sets value for 'transform' attribute
    returns: ?
    '''
    if val:
        node.attrib['transform'] = val


def add_transform(node, val):
    '''
    parameters: node, attribute value
    prepends to or sets 'transform' attribute
    returns: ?
    '''
    if val:
        if 'transform' in node.attrib:
            node.attrib['transform'] = '{0} {1}'.format(val, node.attrib['transform'])
        else:
            set_transform(node, val)


def merge_transform(node, val):
    '''
    parameters: node, attribute value
    merges new transform into existing transform attribute
    returns: new transform (matrix())
    '''
    mat_new = simpletransform.composeTransform(
        simpletransform.parseTransform(val),
        simpletransform.parseTransform(get_transform(node)))
    set_transform(node, simpletransform.formatTransform(mat_new))


def del_transform(node):
    '''
    paramaters: node
    deletes attribute 'transform'
    returns: ?
    '''
    if 'transform' in node.attrib:
        del node.attrib['transform']


def get_parent_transform(node):
    '''
    parameters: node
    gets global matrix transformation for node
    returns: formatted transform attribute value
    '''
    return simpletransform.formatTransform(
        simpletransform.composeParents(node, [[1.0, 0.0, 0.0], [0.0, 1.0, 0.0]]))


def add_translate(node, offset_x, offset_y):
    '''
    parameters: node, offset_x, offset_y
    prepends translate(offset_x, offset_y) to 'transform' attribute
    returns: ?
    '''
    add_transform(node, 'translate({0}, {1})'.format(-1 * offset_x, -1 * offset_y))


def merge_translate(node, offset_x, offset_y):
    '''
    parameters: node, offset_x, offset_y
    merges translate() with existing transformation(s)
    returns: ?
    '''
    transf_str = 'translate({0}, {1})'.format(-1 * offset_x, -1 * offset_y)
    merge_transform(node, transf_str)


def apply_translate(node, offset_x, offset_y):
    '''
    parameters: node, offset_x, offset_y
    applies translation to node attributes if possible, adds preserved transform otherwise
    returns: ?
    '''
    if get_transform(node):
        add_translate(node, offset_x, offset_y)
    else:
        if node.tag in (inkex.addNS('path', 'svg'), 'path'):
            pathdata = simplepath.parsePath(node.get('d'))
            simplepath.translatePath(pathdata, -1 * offset_x, -1 * offset_y)
            node.set('d', simplepath.formatPath(pathdata))
        elif node.tag in (inkex.addNS('rect', 'svg'), 'rect',
                          inkex.addNS('image', 'svg'), 'image'):
            x, y = (float(node.get('x')) - offset_x, float(node.get('y')) - offset_y)
            for key, val in (('x', x), ('y', y)):
                set_or_del_key_val(node, key, val)
        elif node.tag in (inkex.addNS('circle', 'svg'), 'circle',
                          inkex.addNS('ellipse', 'svg'), 'ellipse'):
            cx, cy = (float(node.get('cx')) - offset_x, float(node.get('cy')) - offset_y)
            for key, val in (('cx', cx), ('cy', cy)):
                set_or_del_key_val(node, key, val)
        elif node.tag in (inkex.addNS('line', 'svg'), 'line'):
            x1, y1 = (float(node.get('x1')) - offset_x, float(node.get('y1')) - offset_y)
            x2, y2 = (float(node.get('x2')) - offset_x, float(node.get('y2')) - offset_y)
            for key, val in (('x1', x1), ('y1', y1), ('x2', x2), ('y2', y2)):
                set_or_del_key_val(node, key, val)
        else:
            add_translate(node, offset_x, offset_y)


def add_scale(node, scale_x, scale_y=None):
    '''
    parameters: node, scale_x, scale_y
    prepends scale(scale_x, scale_y) to 'transform' attribute
    returns: ?
    '''
    if scale_y is None:
        scale_y = scale_x
    add_transform(node, 'scale({0}, {1})'.format(scale_x, scale_y))


def merge_scale(node, scale_x, scale_y=None):
    '''
    parameters: node, scale_x, scale_y
    merges scale() with existing transformation(s)
    returns: ?
    '''
    if scale_y is None:
        scale_y = scale_x
    transf_str = 'scale({0}, {1})'.format(scale_x, scale_y)
    merge_transform(node, transf_str)


def apply_scale(node, scale_x, scale_y=None):
    '''
    parameters: node, scale_x, scale_y
    applies scale to node attributes if possible, adds preserved transform otherwise
    returns: ?
    '''
    if get_transform(node):
        add_scale(node, scale_x, scale_y)
    else:
        if scale_y is None:
            scale_y = scale_x
        if node.tag in (inkex.addNS('path', 'svg'), 'path'):
            pathdata = simplepath.parsePath(node.get('d'))
            simplepath.scalePath(pathdata, scale_x, scale_y)
            node.set('d', simplepath.formatPath(pathdata))
        elif node.tag in (inkex.addNS('rect', 'svg'), 'rect',
                          inkex.addNS('image', 'svg'), 'image'):
            # TODO: scale rectangles and images (width, height)
            add_scale(node, scale_x, scale_y)
        else:
            add_scale(node, scale_x, scale_y)


def add_flip(node, direction, offset):
    '''
    parameters: node, direction, offset
    prepends transforms to flip node (vertically or horizontally to 'transform' attribute
    returns: ?
    '''
    if direction == 'y':
        transf_str = 'translate(0, {}) scale(1,-1)'.format(offset)
    elif direction == 'x':
        transf_str = 'translate({}, 0) scale(-1,1)'.format(offset)
    add_transform(node, transf_str)


def merge_flip(node, direction, offset):
    '''
    parameters: node, direction, offset
    merges flip transformation with existing transformation(s)
    returns: ?
    '''
    if direction == 'y':
        transf_str = 'translate(0, {}) scale(1,-1)'.format(offset)
    elif direction == 'x':
        transf_str = 'translate({}, 0) scale(-1,1)'.format(offset)
    merge_transform(node, transf_str)


def apply_flip(node, direction, offset):
    '''
    parameters: node, direction, offset
    flips and translates node based on direction and offset
    returns: ?
    '''
    if get_transform(node):
        add_flip(node, direction, offset)
    else:
        if node.tag in (inkex.addNS('path', 'svg'), 'path'):
            pathdata = simplepath.parsePath(node.get('d'))
            if direction == 'y':
                simplepath.scalePath(pathdata, 1, -1)
                simplepath.translatePath(pathdata, 0, offset)
            elif direction == "x":
                simplepath.scalePath(pathdata, -1, 1)
                simplepath.translatePath(pathdata, offset, 0)
            node.set('d', simplepath.formatPath(pathdata))
        else:
            add_flip(node, direction, offset)


# ------------------------------------------------------------------- #
# helper functions for SVG import as groups
# ------------------------------------------------------------------- #

def get_files_recursive(path, suffix, ignore_symlinks=True):
    ''' returns list of files found in walked path '''
    files = []
    if locale.getpreferredencoding():
        dir_locale = locale.getpreferredencoding()
    else:
        dir_locale = "UTF-8"
    path = unicode(path, dir_locale)
    if os.path.isdir(path):
        for d, dn, fns in os.walk(path):
            for fn in fns:
                if DEBUG:
                    inkex.errormsg(u'{}: {}'.format(fn, type(fn)))
                fp = os.path.join(d, fn)
                if ignore_symlinks and os.path.islink(fp):
                    pass
                elif fn.endswith('.{}'.format(suffix)):
                    files.append(fp)
        return files
    else:
        inkex.debug('Path "{}" does not exist'.format(path))


def get_files_flat(path, suffix, ignore_symlinks=True):
    ''' returns: list of files found in path '''
    files = []
    if locale.getpreferredencoding():
        dir_locale = locale.getpreferredencoding()
    else:
        dir_locale = "UTF-8"
    path = unicode(path, dir_locale)
    if os.path.isdir(path):
        for fn in os.listdir(path):
            if DEBUG:
                inkex.errormsg(u'{}: {}'.format(fn, type(fn)))
            fp = os.path.join(path, fn)
            if os.path.isfile(fp):
                if ignore_symlinks and os.path.islink(fp):
                    pass
                elif fn.endswith('.{}'.format(suffix)):
                    files.append(fp)
        return files
    else:
        inkex.debug('Path "{}" does not exist'.format(path))


def sorted_by_filename(files):
    ''' returns list of filepaths sorted by filename '''
    return sorted(files, key=lambda x: os.path.split(x)[1])


def sanitize_string(text):
    ''' replace unicode with ascii, other characters in *text* from list with _ '''
    text_orig = text
    if isinstance(text, unicode):
        text = unidecode(text)
    chars = "\\`'*{}[]()<>#+.,;:!?|$ %"
    for c in chars:
        if c in text:
            text = text.replace(c, "_")
    DEBUG and inkex.errormsg(u'{} -> {}'.format(text_orig, text))
    return text


def get_basename(filepath):
    ''' returns basename with suffix stripped of filepath '''
    return os.path.splitext(os.path.basename(filepath))[0]


def parse_svg_file(filepath):
    '''
    parameters: filename (full path)
    parses svgfile with etree
    returns: doc
    '''
    if os.path.isfile(filepath):
        p = inkex.etree.XMLParser(huge_tree=True)
        with open(filepath, 'r') as stream:
            return inkex.etree.parse(stream, parser=p)
    else:
        inkex.errormsg('File "{}" does not exist'.format(filepath))


def _unit2csspx(v, u):
    '''Returns quantity in CSS Pixel of given *value* in *unit* '''
    return v * UUCONV[u]


def _csspx2unit(v, u):
    ''' Returns quantity in *unit* of given *value* in CSS Pixel '''
    return v / UUCONV[u]


def _unit2unit(v, u1, u2):
    ''' Returns quantity in *unit2* of given *value* in *unit1* '''
    return (v * UUCONV[u1]) / UUCONV[u2]


def get_viewbox(node):
    '''
    parameters: node (document root)
    gets viewbox string from node, splits into values
    returns: list of floats (x, y, w, h)
    '''
    vbx = vby = vbw = vbh = None
    vbstr = node.get('viewBox', None)
    if vbstr is not None:
        try:
            vbx, vby, vbw, vbh = (float(i) for i in vbstr.split())
        except TypeError, error_msg:
            if DEBUG:
                inkex.debug(error_msg)
        except ValueError, error_msg:
            if DEBUG:
                inkex.debug(error_msg)
    return (vbx, vby, vbw, vbh)


def get_size_attributes(node):
    '''
    parameters: node (document root)
    gets width and height of node, splits into values and unit identifiers
    returns: list of floats and strings (width, unit, height, unit)
    '''
    uuconv_local = dict(UUCONV)
    uuconv_local['%'] = None
    uuconv_local['em'] = None
    wv = hv = wu = hu = None
    width = node.get('width')
    height = node.get('height')
    value_match = re.compile(r'(([-+]?[0-9]+(\.[0-9]*)?|[-+]?\.[0-9]+)([eE][-+]?[0-9]+)?)')
    unit_match = re.compile('(%s)$' % '|'.join(uuconv_local.keys()))
    if width is not None:
        mwv = value_match.match(width)
        if mwv:
            wv = mwv.group()
        mwu = unit_match.search(width)
        if mwu:
            wu = mwu.group()
    if height is not None:
        mhv = value_match.match(height)
        if mhv:
            hv = mhv.group()
        mhu = unit_match.search(height)
        if mhu:
            hu = mhu.group()
    # unset if relative units:
    wv = (None if wu in ["%", "em"] else wv)
    hv = (None if hu in ["%", "em"] else hv)
    # unset if invalid values (not numbers)
    try:
        wv, hv = (float(i) for i in (wv, hv))
    except TypeError:
        wv = wu = hv = hu = None
    return (wv, wu, hv, hu)


def get_drawing_scale(svgdoc):
    '''
    parameters: svgdoc (XML tree)
    calculate drawing scale (size of user units based on width/height)
    returns: dict (viewport)
    '''
    rt = svgdoc.getroot()
    viewport = {}
    viewport['viewbox'] = get_viewbox(rt)
    viewport['page'] = get_size_attributes(rt)
    scale_x = scale_y = 1.0
    # TODO: support scale based on preserveAspectRatio, meetOrSlice of root
    if viewport['viewbox'][2] is not None:
        if viewport['page'][0] is not None:
            if viewport['page'][1] is None or viewport['page'][1] == 'px':
                scale_x = viewport['page'][0] / viewport['viewbox'][2]
            else:
                width_in_px = _unit2csspx(viewport['page'][0], viewport['page'][1])
                scale_x = width_in_px / viewport['viewbox'][2]
            if viewport['page'][3] is None or viewport['page'][3] == 'px':
                scale_y = viewport['page'][2] / viewport['viewbox'][3]
            else:
                height_in_px = _unit2csspx(viewport['page'][2], viewport['page'][3])
                scale_y = height_in_px / viewport['viewbox'][3]
    viewport['aspect'] = [scale_x, scale_y]
    # inkex.debug(viewport)
    return viewport


# ------------------------------------------------------------------- #
# helper functions for symbol export as SVG file
# ------------------------------------------------------------------- #

def run(command_format, prog_name, verbose=False):
    """run command"""
    if verbose:
        inkex.debug(command_format)
    msg = None
    try:
        try:
            from subprocess import Popen, PIPE
            if isinstance(command_format, list):
                p = Popen(command_format, shell=False, stdout=PIPE, stderr=PIPE)
                out, err = p.communicate()
            elif isinstance(command_format, str):
                p = Popen(command_format, shell=True, stdout=PIPE, stderr=PIPE)
                out, err = p.communicate()
            else:
                msg = "unsupported command format %s" % type(command_format)
        except ImportError:
            msg = "subprocess.Popen not available"
        # if err and msg is None:
        #     msg = "%s failed:\n%s\n%s\n" % (prog_name, out, err)
    except Exception, inst:
        msg = "Error attempting to run %s: %s" % (prog_name, str(inst))
    if msg is None:
        return out
    else:
        inkex.errormsg(msg)
        sys.exit(1)


def query_bboxes(svg_file):
    '''
    parameters: svg file
    queries inkscape on the command line for list of all objects
    returns: dict with id as key, bbox (list) as value
    '''
    bboxes = {}
    opts = ['inkscape', '--query-all']
    opts.append('{}'.format(svg_file))
    reader = csv.reader(run(opts, 'inkscape', verbose=False).split(os.linesep))
    for line in reader:
        if len(line):
            bboxes[line[0]] = map(float, line[1:])
    return bboxes


def get_query_bbox(scopes, node):
    '''
    parameters: node
    look up id of node in scopes['bboxes']
    returns: list of bbox values
    '''
    if 'bboxes' not in scopes:
        scopes['bboxes'] = query_bboxes(scopes['svg_file'])
    return scopes['bboxes'][get_id(node)]


def get_approx_bbox(alist):
    '''
    parameters: a list
    try to return bbox for objects in alist
    return: list of values for bbox extents
    '''
    # FIXME: catch errors from computeBBox, provide fallback value
    bbox = simpletransform.computeBBox(alist)
    if bbox:
        xmin, xMax, ymin, yMax = list(bbox)
        return [xmin, ymin, xMax - xmin, yMax - ymin]
    else:
        inkex.debug('computeBBox() failed (list of {} objects)'.format(len(alist)))
        for ele in alist:
            inkex.debug(ele.get('id'))


def create_svg_elementTree():
    '''
    parameters: none
    creates empty ElementTree, with namespaces known from inkex.NSS
    returns: ElementTree
    '''
    doc_name = inkex.etree.QName('svg')
    doc_root = inkex.etree.Element(doc_name, nsmap=NS_MAP)
    doc_root.set('version', "1.1")
    comment_str = " Created with Inkscape (http://www.inkscape.org/) and symbol_tools.py "
    doc_root.addprevious(inkex.etree.Comment(comment_str))
    return inkex.etree.ElementTree(doc_root)


def get_export_template():
    '''
    parameters: none
    returns empty XML tree for new Inkscaep SVG file
    returns: XML etree element
    '''
    doc_template = ('<svg xmlns="%s" xmlns:sodipodi="%s" xmlns:inkscape="%s" \
                     xmlns:cc="%s" xmlns:rdf="%s" xmlns:dc="%s"></svg>'
                    % (inkex.NSS['svg'], inkex.NSS['sodipodi'], inkex.NSS['inkscape'],
                       inkex.NSS['cc'], inkex.NSS['rdf'], inkex.NSS['dc']))
    return inkex.etree.parse(StringIO(doc_template))


def copy_xpath_data(path, source, target):
    '''
    parameters: xpath expression, orig root, new root
    copies data based on xpath expression from source to target
    returns: target
    '''
    data_source = source.xpath(path, namespaces=inkex.NSS)[0]
    target.append(deepcopy(data_source))
    return target


def write_svg_file(path, basename, tree, context=None):
    '''
    parameters: path, basename, tree (XML tree for output)
    dump XML tree as text into new file
    return: ?
    '''
    if os.path.isdir(path):
        filename = '{}.svg'.format(basename)
        if context is not None:
            path = os.path.join(path, context)
        filepath = os.path.join(path, filename)
        with open(filepath, 'w') as output_file:
            output_file.write(inkex.etree.tostring(tree,
                                                   xml_declaration=True,
                                                   encoding="UTF-8",
                                                   pretty_print=True))
    else:
        inkex.errormsg('Export path "{}" does not exist'.format(path))


# ------------------------------------------------------------------- #
# helper functions: convert group <-> symbol
# ------------------------------------------------------------------- #

def get_suffixed_id(node):
    '''
    parameters: node
    appends suffix to id of node
    returns: appended id string
    '''
    return '{}{}'.format(get_id(node), SYMBOL_ID_SUFFIX)


def set_suffixed_id(target, source=None):
    '''
    parameters: target, source (optional)
    sets id of target with SYMBOL_ID_SUFFIX (to id of source) appended
    returns: ?
    '''
    if source is not None:
        set_id(target, get_suffixed_id(source))
    else:
        set_id(target, get_suffixed_id(target))


def get_stripped_id(node):
    '''
    parameters: node
    strips last occurrance of SYMBOL_ID_SUFFIX from id string
    returns: stripped id string
    '''
    return get_id(node).rsplit(SYMBOL_ID_SUFFIX, 1)[0]


def set_stripped_id(target, source=None):
    '''
    parameters: target, source (optional)
    sets id of target with SYMBOL_ID_SUFFIX (of source) stripped
    returns: ?
    '''
    if source is not None:
        set_id(target, get_stripped_id(source))
    else:
        set_id(target, get_stripped_id(target))


# ------------------------------------------------------------------- #
# helper functions for layout
# ------------------------------------------------------------------- #

def reset_origin(node):
    '''
    parameters: group
    checks for transform attribute, deletes it if present
    returns: ?
    '''
    if 'transform' in node.attrib:
        del node.attrib['transform']


def reset_x_y(node):
    '''
    parameters: node
    Checks for presence of x, y attributes and deletes them if found
    returns: ?
    '''
    if 'x' in node.attrib:
        del node.attrib['x']
    if 'y' in node.attrib:
        del node.attrib['y']


# ------------------------------------------------------------------- #
# helper functions for on-canvas text labels
# ------------------------------------------------------------------- #

def get_node_text_label(parent, node):
    '''
    parameters: parent (current layer), referenced node (<title>)
    Searches for <text> objects referencing node, adds one if None found
    returns: text element
    '''
    if LABEL_MODE == "tspan":
        return get_text_with_id_ref(parent, node)
    else:  # if LABEL_MODE == "tref"
        return get_text_with_tref(parent, node)


def del_node_text_label(parent, node):
    '''
    parameters: parent (current layer), referenced node
    Searches in parent for <text> objects referencing node, deletes first result
    returns: ?
    '''
    if LABEL_MODE == "tspan":
        del_text_with_id_ref(parent, node)
    else:  # LABEL_MODE == "tref"
        del_text_with_tref(parent, node)


# vim: expandtab shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=99
