#!/usr/bin/env python
'''
symbol_tools.py - conversion tools for icon and symbol sets

Copyright (C) 2015, ~suv <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
'''
# pylint: disable=invalid-name
# pylint: disable=missing-docstring
# pylint: disable=too-many-lines
# pylint: disable=too-many-statements
# pylint: enable=fixme

# system modules
from math import sqrt, ceil
# internal modules
import inkex
import simplestyle
import symbol_utils as util
import yaml_util
import json_util
import css_util

try:
    import playground
except ImportError:
    pass


# ------------------------------------------------------------------- #
# globals
# ------------------------------------------------------------------- #


# ------------------------------------------------------------------- #
# utility functions
# ------------------------------------------------------------------- #

def formatted(f):
    if f is not None:
        return format(f, '.8f').rstrip('0').rstrip('.')


# ------------------------------------------------------------------- #
# helper functions for svg import
# ------------------------------------------------------------------- #

def add_viewport_rect(viewport):
    '''
    parameters: SVG doc
    creates rect based on viewport info of SVG doc
    returns: rect element
    '''
    vbx, vby, vbw, vbh = viewport['viewbox']
    width, _, height, _ = viewport['page']
    rect = inkex.etree.Element(inkex.addNS('rect', 'svg'))
    try:
        rect.set('x', formatted(vbx))
        rect.set('y', formatted(vby))
        rect.set('width', formatted((vbw)))
        rect.set('height', formatted(vbh))
    except TypeError, error_msg:
        if util.DEBUG:
            inkex.debug(error_msg)
        try:
            rect.set('width', formatted(width))
            rect.set('height', formatted(height))
        except TypeError, error_msg:
            if util.DEBUG:
                inkex.debug(error_msg)
    rect.set('style', "fill:none;stroke:none")
    rect.set(inkex.addNS('label', 'inkscape'), "viewport_rect")
    return rect


def set_viewport(svgdoc, viewport_rect=True, use_viewbox=False):
    '''
    parameters: svgdoc (XML tree of SVG file to be imported as group), flag for viewport rect
    retrieves viewbox and width/height of svgdoc, creates group and adds viewport rect
    returns: group

    TODO: add option to write viewBox for <symbol> definition instead of transforms
          (Needs inkscape to be able to set width, height on the instanciated symbol.
          Currently Inkscape adds width="100%" height="100%" unconditionally.
          Ideally, the <symbol> dialog gets an option for max width (or height) which
          sets the initial size for the <use> element, and snapping is fixed for content
          of <use> elements of <symbols> which are scaled down (similar to nested <svg>
          elements))

    '''
    if not use_viewbox:
        viewport = util.get_drawing_scale(svgdoc)
        svg_group = inkex.etree.Element(inkex.addNS('g', 'svg'))
        if viewport_rect:
            svg_group.append(add_viewport_rect(viewport))
        transf = ""
        scale_x, scale_y = viewport['aspect']
        if scale_x != 1.0 or scale_y != 1.0:
            transf = 'scale({}, {})'.format(scale_x, scale_y)
        offset_x, offset_y = viewport['viewbox'][:2]
        if offset_x or offset_y:
            transf = '{} translate({}, {})'.format(transf, -1 * offset_x, -1 * offset_y)
        if transf:
            util.set_transform(svg_group, transf)
        return svg_group
    else:
        inkex.debug("'use_viewbox' option not yet implemented - falling back to transforms.")
        set_viewport(svgdoc, viewport_rect, False)


def clean_styles(obj, to_delete=False):
    '''
    parameters: obj, flag (optional)
    deletes or unsets certain style properties (linked paint servers) of obj
    returns: flag (to_delete)
    '''
    style = simplestyle.parseStyle(obj.get('style'))
    for adict in obj.attrib, style:
        # unset fills with urls (gradient, pattern, swatch), reduce fill-opacity
        if 'fill' in adict and adict['fill'][:5] == 'url(#':
            del adict['fill']
            if 'fill-opacity' in adict:
                adict['fill-opacity'] = str(max([float(adict['fill-opacity']) - 0.5, 0]))
            else:
                adict['fill-opacity'] = str(0.5)
        # set stroke color to #000000 instead of url (gradient, pattern)
        if 'stroke' in adict and adict['stroke'][:5] == 'url(#':
            adict['stroke'] = "#000000"
        # remove filter effects (require <defs>)
        if 'filter' in adict and adict['filter'][:5] == 'url(#':
            # del adict['filter']
            to_delete = True
    obj.set('style', simplestyle.formatStyle(style))
    return to_delete


def clean_attributes(obj, to_delete=False):
    '''
    parameters: obj, flag (optional)
    deletes certain custom attributes and style properties of obj
    returns: flag (to_delete)
    '''
    # purge some sodipodi and inkscape attributes
    purge_attributes = [
        inkex.addNS('nodetypes', 'sodipodi'),
        inkex.addNS('groupmode', 'inkscape'),
        inkex.addNS('connector-curvature', 'inkscape'),
    ]
    for pa in purge_attributes:
        if pa in obj.attrib:
            del obj.attrib[pa]
    return clean_styles(obj, to_delete)


def clean_objects(node):
    '''
    parameters: node (group)
    iterates all child elements, deletes certain object types, attributes and properties
    returns: node
    '''
    path = './/*'
    objs = node.xpath(path, namespaces=inkex.NSS)
    for obj in objs:
        to_delete = False
        if obj.get(inkex.addNS('label', 'inkscape')) == "viewport_rect":
            continue
        if obj.tag == inkex.addNS('use', 'svg'):
            to_delete = True
        else:
            to_delete = clean_attributes(obj, to_delete)
        if to_delete:
            obj.getparent().remove(obj)
    return node


def set_metadata(node, scopes):
    '''
    parameters: node (doc root), dict with options
    adds metadata to node based on options in scopes
    returns: ?

  <metadata>
    <rdf:RDF>
      <cc:Work rdf:about="">
        <dc:format>image/svg+xml</dc:format>
        <dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage" />

        <!-- scopes['title'] -->
        <dc:title>The title</dc:title>

        <!-- scopes['md_creator'] -->
        <dc:creator>
          <cc:Agent>
            <dc:title>The author's name</dc:title>
          </cc:Agent>
        </dc:creator>

        <!-- scopes['md_publisher'] -->
        <dc:publisher>
          <cc:Agent>
            <dc:title>The publisher web site</dc:title>
          </cc:Agent>
        </dc:publisher>

        <!-- scopes['md_source'] -->
        <dc:source>The download URL or repo</dc:source>

        <!-- scopes['md_desc'] -->
        <dc:description>Description of the content</dc:description>

      </cc:Work>
    </rdf:RDF>
  </metadata>

    '''
    if 'title' in scopes and scopes['title']:
        # Inkscape always copies title of SVG root to the dublincore meatadata
        util.set_title(node, scopes['title'])
    # TODO: implement option to fill in other metadata
    if 'dc_creator' in scopes and scopes['dc_creator']:
        pass
    if 'dc_publisher' in scopes and scopes['dc_publisher']:
        pass
    if 'dc_source' in scopes and scopes['dc_source']:
        pass
    if 'dc_desc' in scopes and scopes['dc_desc']:
        pass


# ------------------------------------------------------------------- #
# helper functions: export
# ------------------------------------------------------------------- #

def get_export_size(scopes, group, use_query=False):
    '''
    parameters: group
    if element with label 'em_ref' is found, use for bbox, else return bbox of group
    returns: bbox (xmin, ymin, width, height)
    '''
    path = './/svg:path[@inkscape:label="em_ref"]'
    result = group.xpath(path, namespaces=inkex.NSS)
    if not len(result):
        result = [group]
    if use_query:
        return util.get_query_bbox(scopes, result[0])
    else:
        return util.get_approx_bbox(result)


def get_em_ref_size(group):
    '''
    parameters: group
    if element with label 'em_ref' is found, use for bbox, else return bbox of group
    returns: bbox (xmin, xMax, ymin, yMax)
    '''
    path = './/svg:path[@inkscape:label="em_ref"]'
    result = group.xpath(path, namespaces=inkex.NSS)
    if not len(result):
        result = [group]
    return util.get_approx_bbox(result)


def translate_node_children_to_origin(scopes, source, node, use_query=False):
    '''
    parameters: scopes, source (original node), node, use_query flag (optional)
    translates node children to SVG origin (0,0)
    returns: ?
    '''
    if use_query:
        parent_transf = util.get_parent_transform(source)
        xmin, ymin = list(util.get_query_bbox(scopes, source))[:2]
        if util.is_group(node):
            # FIXME: only delete 'translate()' component (?)
            util.del_transform(node)
            for item in node.iterchildren():
                util.merge_translate(item, xmin, ymin)
                if parent_transf:
                    util.merge_transform(item, parent_transf)
        else:
            util.merge_translate(node, xmin, ymin)
            if parent_transf:
                util.merge_transform(node, parent_transf)
    else:
        if util.is_group(node):
            # FIXME: only delete 'translate()' component (?)
            util.del_transform(node)
            xmin, ymin = list(util.get_approx_bbox([node]))[:2]
            for item in node.iterchildren():
                util.apply_translate(item, xmin, ymin)
        else:
            xmin, ymin = list(util.get_approx_bbox([node]))[:2]
            util.apply_translate(node, xmin, ymin)


# ------------------------------------------------------------------- #
# helper functions for layout
# ------------------------------------------------------------------- #

def fit_node_to_area(node, area, offset=None):
    '''
    paramaters: node (document root), area (width, height)
    Adjusts node width, height and viewBox attributes to table size
    returns: ?
    '''
    if offset is None:
        offset = [0, 0]
    node.set('width', str(area[0]))
    node.set('height', str(area[1]))
    node.set('viewBox', '{0} {1} {2} {3}'.format(offset[0], offset[1], area[0], area[1]))


def get_cell_origins(count, layout):
    '''
    parameters: object count, layout options
    creates list of transforms to store count objects in rows and columns
    returns: list of points (to be used for 'translate()')
    '''
    coord_list = []
    table_area = []
    offset = [layout['offset_x'], layout['offset_y']]
    cell = [layout['cell_width'], layout['cell_height']]
    width_factor = layout['width_factor']
    if layout['direction'] == "horizontal":
        if layout['per_row'] == 0:
            per_row = int(ceil(sqrt(count)))
            per_column = int(ceil(sqrt(count)))
        else:
            per_row = int(layout['per_row'])
            per_column = int(ceil(count / float(per_row)))
        for j in range(per_column):
            for i in range(per_row):
                coord_list.append([offset[0] + (i * cell[0]),
                                   offset[1] + (j * cell[1])])
        table_area = [(per_row * cell[0]) + (2 * offset[0]),
                      (per_column * cell[1]) + (2 * offset[1])]
    elif layout['direction'] == "vertical":
        if layout['per_row'] == 0:
            per_row = int(ceil(sqrt(count) / width_factor * sqrt(width_factor)))
            per_column = int(ceil(count / float(per_row)))
        else:
            per_row = int(layout['per_row'])
            per_column = int(ceil(count / float(per_row)))
        for i in range(per_row):
            for j in range(per_column):
                coord_list.append([offset[0] + (i * cell[0] * width_factor),
                                   offset[1] + (j * cell[1])])
        table_area = [(per_row * cell[0] * width_factor) + (2 * offset[0]),
                      (per_column * cell[1]) + (2 * offset[1])]
    return (coord_list, table_area)


def get_dict_with_id_of(alist, defs):
    '''
    parameters: a list, defs
    Creates dict based on element type of list items, using id as key (for sorting)
    returns: a dict
    '''
    adict = {}
    for node in alist:
        if util.is_group(node):
            adict[util.get_id(node)] = (node, node)
        elif util.is_use(node) and defs is not None:
            s_def = util.get_symbol_from_instance(node, defs)
            adict[util.get_id(s_def)] = (node, s_def)
    return adict


# ------------------------------------------------------------------- #
# helper functions for copying labels
# ------------------------------------------------------------------- #

def wrap_in_group(node):
    '''
    parameters: node
    Wraps node in new group, copies id from node to group
    returns: group (new node)
    '''
    node_parent = node.getparent()
    node_index = node_parent.index(node)
    node_group = inkex.etree.Element(inkex.addNS('g', 'svg'))
    node_parent.insert(node_index, node_group)
    node_group.append(node)
    util.set_id(node_group, util.get_id(node))
    util.del_id(node)
    return node_group


def get_source_vals(node, source):
    '''
    parameters: node, source option
    parses source option and retreives needed attr values and element texts
    returns: list of text strings
    '''
    source_list = []
    if source == 'id':
        source_list.append(util.get_id(node))
    elif source == 'label':
        source_list.append(util.get_label(node))
    elif source == 'title':
        source_list.append(util.get_title_text(node))
    elif source == 'all':
        source_list.append(util.get_id(node))
        source_list.append(util.get_label(node))
        source_list.append(util.get_title_text(node))
    return source_list


def del_source_labels(node, source):
    '''
    parameters: node, source option
    parses source option and deletes attributes or child elements (<title>)
    returns: ?
    '''
    if source == 'id':
        util.del_id(node)
    elif source == 'label':
        util.del_label(node)
    elif source == 'title':
        util.del_title(node)
    elif source == 'all':
        util.del_id(node)
        util.del_label(node)
        util.del_title(node)


def set_target_vals(node, source, source_list, target):
    '''
    parameters: node, source option, list of source values, target option
    sets attributes and title text of node from source_list, based on source and target option
    returns: ?
    '''
    source_val = source_list[0]
    if target == 'id':
        util.set_id(node, source_val)
    elif target == 'label':
        util.set_label(node, source_val)
    elif target == 'title':
        util.set_title_text(node, source_val)
    elif target == 'other' and source != 'all':
        if source != 'id':
            util.set_id(node, source_val)
        if source != 'label':
            util.set_label(node, source_val)
        if source != 'title':
            util.set_title_text(node, source_val)
    elif target == 'all':
        if source == 'all':
            util.set_id(node, source_list[0])
            util.set_label(node, source_list[1])
            util.set_title_text(node, source_list[2])
        else:
            util.set_id(node, source_val)
            util.set_label(node, source_val)
            util.set_title_text(node, source_val)


# ------------------------------------------------------------------- #
# helper functions: svgfonts
# ------------------------------------------------------------------- #

def get_svgfont_info(scopes, defs):
    '''
    parameters: scopes, defs
    extracts relevant attribute values from svgfont-related elements
    returns: dict
    '''
    svgfont = {}
    svgfont['font'] = defs.find(inkex.addNS('font', 'svg'))
    svgfont['fontface'] = svgfont['font'].find(inkex.addNS('font-face', 'svg'))
    svgfont['baseline'] = 0
    if 'horiz-origin-y' in svgfont['font'].attrib:
        svgfont['baseline'] = svgfont['font'].get('horiz-origin-y')
    svgfont['advance_x'] = svgfont['font'].get('horiz-adv-x')
    svgfont['emsize'] = svgfont['fontface'].get('units-per-em')
    svgfont['scale_factor'] = scopes['basetile'] / float(svgfont['emsize'])
    return svgfont


def get_svgfont_transforms(svgfont):
    '''
    parameters: dict (svgfont info)
    prepare strings required for transform attributes
    returns: dict
    '''
    transf = {}
    transf['flip_y_offset'] = int(svgfont['emsize']) - int(svgfont['baseline'])
    transf['scale_x'] = svgfont['scale_factor']
    transf['flip'] = 'translate(0, {}) scale(1,-1)'.format(transf['flip_y_offset'])
    transf['scale'] = 'scale({})'.format(transf['scale_x'])
    return transf


def get_glyph_mapping(scopes):
    '''
    parameters: scopes
    creates dict with mapping of unicode codes to glyph/icon names
    returns: dict
    '''
    if scopes['mapping'][0] == 'glyphname':
        scopes['use_glyphname'] = scopes['glyphname_as_title']
    elif scopes['mapping'][0] == 'namelist':
        pass
    elif scopes['mapping'][0] == 'yaml':
        return yaml_util.parse_config_file(scopes['mapping'][1])
    elif scopes['mapping'][0] == 'json':
        return json_util.parse_config_file(scopes['mapping'][1])
    elif scopes['mapping'][0] == 'css':
        return css_util.parse_config_file(scopes['mapping'][1])
    elif scopes['mapping'][0] == 'csv':
        pass
    elif scopes['mapping'][0] == 'dict':
        pass
    elif scopes['mapping'][0] == 'presets':
        pass


def get_glyph_info(svgfont, glyphs_dict, unicode_char):
    '''
    parameters: dict (glyph mapping), unicode char
    checks whether unicode is mapped to icon name, returns dict with icon info
    returns: dict
    '''
    if isinstance(unicode_char, unicode) and len(unicode_char) > 1:
        # FIXME: ord() failed with SymbolaC0 SVG font
        if len(unicode_char) == 2:
            unicode_str = '{}{}'.format(hex(ord(unicode_char[0])), hex(ord(unicode_char[1])))
        else:
            unicode_str = ""
    else:
        try:
            unicode_str = hex(ord(unicode_char))
        except TypeError:
            unicode_str = unicode_char
    glyph_info = {
        'id': '{}_{}'.format(util.get_id(svgfont['font']), unicode_str),
        'name': unicode_str,
    }
    if glyphs_dict is not None:
        try:
            glyph_info = glyphs_dict[unicode_str[2:]]
        except KeyError:
            pass
    return glyph_info


def path_to_group(node, parent):
    '''
    parameters: node (glyph), parent (current layer)
    converts glyph to group in current layer (parent)
    returns: group
    '''
    d = node.get("d")
    if d is not None:
        new_group = util.add_group(parent)
        path = inkex.etree.SubElement(new_group, inkex.addNS('path', 'svg'))
        path.set("d", d)
        return new_group


def draw_em_ref_path(scopes, y, w, h):
    '''
    parameters: scopes, y offset (descent), width (advance-x), height (emsize)
    creates path, draws sub-paths for baseline and em rect
    returns: path
    '''
    path = inkex.etree.Element(inkex.addNS('path', 'svg'))
    path.set(inkex.addNS('label', 'inkscape'), "em_ref")
    y = ("0" if y is None else y)
    # path_d = "M 0,0 Z"
    path_d = "M 0,0 l 0,0 Z"  # workaround for Inkscape's node tool (we only want 1 node)
    if scopes['draw_baseline']:
        path_d = 'M 0,0 l {},0'.format(w)
    if scopes['draw_em_rect']:
        path_d = '{0} M 0,{1} l {2},0 0,{3} -{2},0 z'.format(path_d, y, w, h)
    path.set('d', path_d)
    path.set('style', "fill:none;stroke:none")
    return path


def draw_em_ref_group(scopes, y, w, h):
    '''
    parameters: scopes, y offset (descent), width (advance-x), height (emsize)
    creates group, append rect and line
    returns: group
    '''
    group = inkex.etree.Element(inkex.addNS('g', 'svg'))
    group.set(inkex.addNS('label', 'inkscape'), "em_ref")
    if scopes['draw_baseline']:
        line = inkex.etree.Element(inkex.addNS('line', 'svg'))
        line.set('x2', w)
        line.set('style', "fill:none;stroke:none")
        group.append(line)
    if scopes['draw_em_rect']:
        rect = inkex.etree.Element(inkex.addNS('rect', 'svg'))
        if y is not None:
            rect.set('y', y)
        rect.set('width', w)
        rect.set('height', h)
        rect.set('style', "fill:none;stroke:none")
        group.append(rect)
    return group


def add_glyph_em_ref(scopes, svgfont, glyph):
    '''
    parameters: scopes, svgfont, glyph
    creates rect, line based on em size (height), advance-x (width), descent (y-offset)
    returns: group or path element (inkscape label: 'em_ref')
    '''
    if scopes['ignore_descent']:
        em_ref_offset_y = None
    else:
        em_ref_offset_y = svgfont['fontface'].get("descent")
    if 'horiz-adv-x' in glyph.attrib:
        em_ref_width = glyph.get('horiz-adv-x')
    elif 'advance_x' in svgfont:
        em_ref_width = svgfont['advance_x']
    else:
        em_ref_width = scopes['root'].get('width')
    if 'emsize' in svgfont:
        em_ref_height = svgfont['emsize']
    else:
        em_ref_height = scopes['root'].get('height')
    if scopes['flatten_transforms']:
        return draw_em_ref_path(scopes, em_ref_offset_y, em_ref_width, em_ref_height)
    else:
        return draw_em_ref_group(scopes, em_ref_offset_y, em_ref_width, em_ref_height)


def glyph_group_transform_apply(glyph_group, transform):
    '''
    parameters: glyph group, dict with transforms
    applies transforms to path data if possible
    returns: glyph group
    '''
    for child in glyph_group.iterchildren():
        util.apply_flip(child, 'y', transform['flip_y_offset'])
        util.apply_scale(child, transform['scale_x'])
    return glyph_group


def glyph_group_transform_flat(glyph_group, transform):
    '''
    parameters: glyph group, dict with transforms
    applies transforms to child elements of glyph group
    returns: glyph group
    '''
    for child in glyph_group.iterchildren():
        util.add_flip(child, 'y', transform['flip_y_offset'])
        util.add_scale(child, transform['scale_x'])
    return glyph_group


def glyph_group_transform_wrapped(group, transform):
    '''
    parameters: glyph group, dict with transforms
    applies transforms to nested groups wrapped around original glyph group
    returns: new glyph group
    '''
    util.add_flip(group, 'y', transform['flip_y_offset'])
    scale_group = wrap_in_group(group)
    util.add_scale(scale_group, transform['scale_x'])
    return wrap_in_group(scale_group)


def transform_glyph_group(scopes, glyph_group, transf):
    '''
    parameters: scopes, glyph group, transf
    dispatcher to apply transforms to glyph group
    returns: new glyph group

    TODO: add option to write viewBox for <symbol> definition instead of transforms
          (Needs inkscape to be able to set width, height on the instanciated symbol.
          Currently Inkscape adds width="100%" height="100%" unconditionally.
          Ideally, the <symbol> dialog gets an option for max width (or height) which
          sets the initial size for the <use> element, and snapping is fixed for content
          of <use> elements of <symbols> which are scaled down (similar to nested <svg>
          elements))

    '''
    if scopes['flatten_transforms']:
        if scopes['apply_transforms']:
            return glyph_group_transform_apply(glyph_group, transf)
        else:
            return glyph_group_transform_flat(glyph_group, transf)
    else:
        return glyph_group_transform_wrapped(glyph_group, transf)


# ------------------------------------------------------------------- #
# functions: convert group <-> symbol
# ------------------------------------------------------------------- #

def symbol_to_group(symbol, layer):
    '''
    parameters: symbol definition, target layer
    converts symbol into group
    returns: group
    '''
    return util.copy_node_to_node(symbol, util.add_group(layer))


def group_to_symbol(group, defs):
    '''
    parameters: group, defs
    converts group into symbol
    returns: symbol
    '''
    # FIXME: check for groups with same id or title (?)
    return util.copy_node_to_node(group, util.add_symbol(defs))


def convert_symbols_to_groups(scopes, layout, defs):
    '''
    converts symbols into groups,
    creates and grid-arranges groups
    returns: area (width/height) of layout or None
    '''
    use_list = util.get_uses(scopes['source_layer'])
    symbol_list = []
    group_list = []
    if use_list:
        for node in use_list:
            def_ = util.get_symbol_from_instance(node, defs)
            if def_ is not None:
                if def_ not in symbol_list:
                    transf = util.get_transform(node)
                    symbol_list.append((def_, transf))
                node.getparent().remove(node)
        for node, transf in symbol_list:
            def_ = symbol_to_group(node, scopes['target_layer'])
            if scopes['fixed_id']:
                util.set_stripped_id(def_)
            if def_ is not None:
                group_list.append((def_, transf))
                node.getparent().remove(node)
        if layout['type'] == "keep":
            for node, transf in group_list:
                util.set_transform(node, transf)
        elif layout['type'] == "grid":
            return arrange_items_in_grid(zip(*group_list)[0], layout)
        elif layout['type'] == "list":
            return arrange_items_in_list(zip(*group_list)[0], layout)


def convert_groups_to_symbols(scopes, layout, defs):
    '''
    converts groups into symbols,
    creates and grid-arranges instances of symbols
    returns: area (width/height) of layout or None
    '''
    group_list = util.get_groups(scopes['source_layer'])
    symbol_list = []
    use_list = []
    if group_list:
        for node in group_list:
            def_ = group_to_symbol(node, defs)
            if scopes['fixed_id']:
                util.set_suffixed_id(def_)
            if def_ is not None:
                transf = util.get_transform(node)
                symbol_list.append((def_, transf))
                node.getparent().remove(node)
        for node, transf in symbol_list:
            inst_ = util.add_instance(node, scopes['target_layer'])
            if scopes['fixed_id']:
                util.set_stripped_id(inst_, node)
            if inst_ is not None:
                use_list.append((inst_, transf))
        if layout['type'] == "keep":
            for node, transf in use_list:
                util.set_transform(node, transf)
            return None
        elif layout['type'] == "grid":
            return arrange_items_in_grid(zip(*use_list)[0], layout, defs)
        elif layout['type'] == "list":
            return arrange_items_in_list(zip(*use_list)[0], layout, defs)


def convert_items(scopes, layout, defs):
    '''
    parameters: scopes, layout, defs
    Converts items based on mode to symbols or to groups
    Returns: area (width/height) of layout, or None
    '''
    if scopes['source'] == 'uses':
        layout['table_area'] = convert_symbols_to_groups(scopes, layout, defs)
    elif scopes['source'] == 'groups':
        layout['table_area'] = convert_groups_to_symbols(scopes, layout, defs)
    elif scopes['source'] == 'selection':
        layout['table_area'] = None


# ------------------------------------------------------------------- #
# functions: import SVG files as groups
# ------------------------------------------------------------------- #

def copy_content(svgdoc, svg_group, copy_defs=False):
    '''
    parameters: svgdoc, svg_group, copy_defs (optional)
    copies children of doc root in svg doc to svg_group
    returns: group
    '''
    for item in svgdoc.getroot().iterchildren():
        # optionally import defs too
        # NOTE: not supported in targeted use as <symbol> elements
        if copy_defs:
            # TODO: explore appending children to top-level <defs>
            # TODO: address duplicate IDs for imported shared resources
            if item.tag == inkex.addNS('defs', 'svg'):
                svg_group.append(item)
            elif item.tag == 'defs':
                # workaround for invalid files without namespace declarations
                svg_group.append(item)
        if item.tag in util.IMPORT_TAGS:
            svg_group.append(item)
        elif item.tag in util.BASIC_SVG_OBJECTS:
            # workaround for invalid files without namespace declarations
            svg_group.append(item)
    if not copy_defs:
        return clean_objects(svg_group)
    else:
        return svg_group


def get_files(scopes, suffix):
    '''
    parameters: scopes, suffix
    dispatcher for file import options
    returns: sorted list of filepaths
    '''
    if scopes['dir_walk']:
        files = util.get_files_recursive(scopes['path'], suffix, scopes['ignore_symlinks'])
    else:
        files = util.get_files_flat(scopes['path'], suffix, scopes['ignore_symlinks'])
    if files is not None:
        if scopes['sort_by'] == 'filename':
            return util.sorted_by_filename(files)
        elif scopes['sort_by'] == 'dirname':
            return sorted(files)


def import_as_groups(scopes):
    '''
    parameters: scopes, layout (optional)
    creates list of SVG files in source dir, imports each as group
    returns: ?
    '''
    group_list = []
    svgfiles = get_files(scopes, 'svg')
    scopes['import_defs'] = (False if scopes['clean_styles'] else True)
    if svgfiles is not None:
        for svgfile in svgfiles:
            svgdoc = util.parse_svg_file(svgfile)
            if svgdoc is not None:
                svg_group = copy_content(svgdoc,
                                         set_viewport(svgdoc, scopes['viewport_rect']),
                                         scopes['import_defs'])
                scopes['target_layer'].append(svg_group)
                if 'transform' in svg_group.attrib:
                    svg_group = wrap_in_group(svg_group)
                util.set_id(svg_group, util.sanitize_string(util.get_basename(svgfile)))
                util.set_title(svg_group, util.get_basename(svgfile))
                group_list.append(svg_group)
    if len(group_list):
        set_metadata(scopes['root'], scopes)


# ------------------------------------------------------------------- #
# functions: export groups, symbols or selection to SVG files
# ------------------------------------------------------------------- #

def cleanup_for_export(node):
    '''
    parameters: node
    remove unnecessary attributes from elements inside node (group)
    returns: ?
    '''
    if node is not None:
        path = './/*[@inkscape:connector-curvature]'
        objs = node.xpath(path, namespaces=inkex.NSS)
        for obj in objs:
            del obj.attrib[inkex.addNS('connector-curvature', 'inkscape')]


def export_object_list(scopes, alist):
    '''
    parameters: scopes, list of objects
    exports all objects in alist to SVG files
    returns: ?
    '''
    for index, node in enumerate(alist):
        export_tree = util.create_svg_elementTree()
        export_root = export_tree.getroot()
        basename = ""
        util.copy_xpath_data('/svg:svg//svg:metadata', scopes['root'], export_root)
        new_group = util.copy_node_to_node(node, util.add_group(export_root), move=False)
        cleanup_for_export(new_group)
        if scopes['position'] == "move_to_origin":
            translate_node_children_to_origin(scopes, node, new_group, use_query=True)
        if scopes['page_size'] == 'bbox':
            x, y, w, h = list(get_export_size(scopes, new_group, use_query=True))
            if (scopes['position'] == 'move_to_origin' or
                scopes['position'] == 'ignore_offset'):
                fit_node_to_area(export_root, (w, h))
            elif scopes['position'] == 'keep':
                # FIXME: offset is wrong if based on get_query_bbox()
                fit_node_to_area(export_root, (w, h), (x, y))
        else:
            export_root.set('width', scopes['root'].get('width', "100%"))
            export_root.set('height', scopes['root'].get('height', "100%"))
            export_root.set('viewBox', scopes['root'].get('viewBox', "0 0 100 100"))
        if 'style' in scopes['root'].attrib:
            export_root.set('style', scopes['root'].get('style'))
        if scopes['basename'] == 'title':  # TODO: could be unicode
            basename = util.get_title_text(node)
        elif scopes['basename'] == 'label':
            basename = util.get_label(node)
        elif scopes['basename'] == 'id':
            basename = util.get_id(node)
        if not basename or scopes['basename'] == 'index':
            basename = '{}_{}'.format(scopes['basename_string'], str(index))
        if basename:
            if scopes['suffix'] == 'symbolic':
                basename = '{}-symbolic'.format(basename)
            elif scopes['suffix'] == 'string':
                # TODO: test
                basename = '{}{}'.format(basename, scopes['suffix_string'])
        inkex.debug(basename)
        if scopes['layer_context']:
            # TODO: unicode in layer's inkscape:label?
            context = scopes['source_layer'].get(inkex.addNS('label', 'inkscape'))
        else:
            context = None
        if basename:
            util.write_svg_file(scopes['path'], basename, export_tree, context)


def export_symbol_list(scopes, alist):
    '''
    parameters: scopes, list of symbol definitions
    exports all symbols in symbol_list to SVG files
    returns: ?
    '''
    for index, node in enumerate(alist):
        export_tree = util.create_svg_elementTree()
        export_root = export_tree.getroot()
        basename = ""
        util.copy_xpath_data('/svg:svg//svg:metadata', scopes['root'], export_root)
        new_group = util.copy_node_to_node(node, util.add_group(export_root), move=False)
        cleanup_for_export(new_group)
        x, y, _, h = list(get_export_size(scopes, new_group))
        fit_node_to_area(export_root, (h, h), (x, y))
        if 'style' in scopes['root'].attrib:
            export_root.set('style', scopes['root'].get('style'))
        if scopes['basename'] == 'title':  # TODO: could be unicode
            basename = util.get_title_text(node)
        elif scopes['basename'] == 'label':
            basename = util.get_label(node)
        elif scopes['basename'] == 'id':
            basename = util.get_id(node)
        if not basename or scopes['basename'] == 'index':
            basename = 'symbol_{}'.format(str(index))
        if basename:
            if scopes['suffix'] == 'symbolic':
                basename = '{}-symbolic'.format(basename)
            elif scopes['suffix'] == 'string':
                # TODO: test
                basename = '{}{}'.format(basename, scopes['suffix_string'])
        inkex.debug(basename)
        if scopes['layer_context']:
            # TODO: unicode in layer's inkscape:label?
            context = scopes['source_layer'].get(inkex.addNS('label', 'inkscape'))
        else:
            context = None
        if basename:
            util.write_svg_file(scopes['path'], basename, export_tree, context)


def export_groups(scopes):
    '''
    parameters: scopes
    export all groups in source layer to individual SVG files
    returns: ?
    '''
    group_list = util.get_groups(scopes['source_layer'])
    if group_list:
        export_object_list(scopes, group_list)


def export_uses(scopes, defs):
    '''
    parameters: scopes, defs
    exports all symbols which have instance on current layer
    returns: ?
    '''
    use_list = util.get_uses(scopes['source_layer'])
    symbol_list = []
    if use_list:
        for node in use_list:
            symbol_def = util.get_symbol_from_instance(node, defs)
            if symbol_def is not None:
                if symbol_def not in symbol_list:
                    symbol_list.append(symbol_def)
    if symbol_list:
        export_symbol_list(scopes, symbol_list)


def export_symbols(scopes, defs):
    '''
    parameters: scopes, defs
    export all <symbol> nodes to individual SVG files
    returns: ?
    '''
    symbol_list = util.get_symbols(defs)
    if symbol_list:
        export_symbol_list(scopes, symbol_list)


def export_selection(scopes):
    '''
    parameters: scopes
    exports all selected objects
    returns: ?
    '''
    if len(scopes['selection']):
        element_list = []
        for id_, ele in scopes['selection'].iteritems():
            if util.is_use(ele):
                inkex.debug('Clone "{}" omitted from selection'.format(id_))
            else:
                element_list.append(ele)
        export_object_list(scopes, element_list)


def export_to_files(scopes):
    '''
    parameters: scopes
    dispatcher for export based on source scope
    returns: ?
    '''
    scopes['bboxes'] = util.query_bboxes(scopes['svg_file'])
    if scopes['source'] == 'groups':
        export_groups(scopes)
    elif scopes['source'] == 'uses':
        export_uses(scopes, scopes['defs'])
    elif scopes['source'] == 'symbol':
        export_symbols(scopes, scopes['defs'])
    elif scopes['source'] == 'selection':
        export_selection(scopes)
    else:
        pass


# ------------------------------------------------------------------- #
# functions: symbols
# ------------------------------------------------------------------- #

def cleanup_symbols(defs):
    '''
    parameters: scopes, defs
    remove unnecessary attributes from elements inside symbol definition
    returns: ?
    '''
    symbol_list = util.get_symbols(defs)
    if symbol_list:
        for node in symbol_list:
            path = './/*[@inkscape:connector-curvature]'
            objs = node.xpath(path, namespaces=inkex.NSS)
            for obj in objs:
                del obj.attrib[inkex.addNS('connector-curvature', 'inkscape')]


def instantiate_symbols(scopes, layout, defs):
    '''
    parameters: scopes, layout (optional), defs
    Creates list of symbols in defs, instatiates clone for each, on target_layer
    returns: ?
    '''
    symbol_list = util.get_symbols(defs)
    use_list = []
    if symbol_list:
        for node in symbol_list:
            transf = None
            inst_ = util.add_instance(node, scopes['target_layer'])
            if scopes['fixed_id']:
                util.set_stripped_id(inst_, node)
            if inst_ is not None:
                use_list.append((inst_, transf))
        if layout is not None:
            # TODO: support layout options for instantiate_symbols()
            pass


def cleanup_items(scopes, defs):
    '''
    parameters: scopes, layout, defs
    remove known unneeded attributes from symbols
    returns: ?
    '''
    if scopes['source'] == 'symbol':
        cleanup_symbols(defs)


def export_use_items(scopes, defs):
    '''
    parameters: scopes, defs
    export all <symbol> elements in defs into separate SVG files
    returns: ?
    '''
    if scopes['source'] == 'symbol':
        export_symbols(scopes, defs)


def clone_items(scopes, layout, defs):
    '''
    parameters: scopes, layout, defs
    creates clones of <symbol> elements in defs
    returns: ?
    '''
    if scopes['source'] == 'symbol':
        instantiate_symbols(scopes, layout, defs)


# ------------------------------------------------------------------- #
# functions: layout
# ------------------------------------------------------------------- #

def arrange_items_in_grid(alist, layout, defs=None):
    '''
    parameters: list of items, layout options, defs (optional)
    Rearranges all items in alist based on cell grid and dimensions (optional)
    Deletes on-canvas text labels of item's <title> if found
    returns: area (width, height) of layout
    '''
    count = len(alist)
    if count:
        coord_list, table_area = get_cell_origins(len(alist), layout)
        for i, node in enumerate(alist):
            transf = 'translate(%s, %s)' % (coord_list[i][0], coord_list[i][1])
            node.set('transform', transf)
            if util.is_group(node):
                util.del_node_text_label(node.getparent(), util.get_title(node))
            elif util.is_use(node) and defs is not None:
                util.del_node_text_label(node.getparent(),
                                         util.get_title(util.get_symbol_from_instance(node, defs)))
        return table_area


def arrange_items_in_list(alist, layout, defs=None):
    '''
    parameters: list of items (elements), layout options, defs (optional)
    Arranges items in sorted list, adds id / title as text objects
    returns: area (width, height) of layout
    '''
    count = len(alist)
    if count:
        adict = {}
        layout['direction'] = "vertical"
        coord_list, table_area = get_cell_origins(count, layout)
        adict = get_dict_with_id_of(alist, defs)
        for i, key in enumerate(sorted(adict.keys())):
            node, def_ = (adict[key][0], adict[key][1])
            transf = 'translate(%s,%s)' % (coord_list[i][0], coord_list[i][1])
            node.set('transform', transf)
            label_text = util.get_node_text_label(node.getparent(), util.add_title(def_))
            if label_text is not None:
                util.reset_x_y(label_text)
                label_transf = 'translate(%s,%s)' % (layout['cell_width'],
                                                     layout['cell_height'] * 0.25)
                label_text.set('transform',
                               '%s %s' % (transf, label_transf))
                label_text.set('style',
                               'stroke:none;font-size:%spx' % (layout['cell_height'] * 0.25))
        return table_area


def arrange_items(scopes, layout, defs=None):
    '''
    parameters: scopes, layout options, defs
    Arranges selection or all items on current layer without conversion
    returns: area (width/height) of layout
    '''
    if scopes['source'] == 'selection':
        objects = scopes['selection'].values()
    elif scopes['source'] == 'uses':
        objects = util.get_uses(scopes['source_layer'])
    elif scopes['source'] == 'groups':
        objects = util.get_groups(scopes['source_layer'])
    if len(objects):
        if layout['type'] == 'grid':
            layout['table_area'] = arrange_items_in_grid(objects, layout, defs)
        elif layout['type'] == 'list':
            layout['table_area'] = arrange_items_in_list(objects, layout, defs)
        elif layout['type'] == 'origin':
            for node in objects:
                util.reset_origin(node)
            layout['table_area'] = (layout['cell_width'] + (2 * layout['offset_x']),
                                    layout['cell_height'] + (2 * layout['offset_y']))


# ------------------------------------------------------------------- #
# functions: copy labels (id, label, title)
# ------------------------------------------------------------------- #

def copy_labels(node, source, target, wrap=False):
    '''
    parameters: selected object, source and target option, wrap option
    Copies source attribute values or node content to target
    returns: ?
    '''
    source_vals = get_source_vals(node, source)
    if wrap:
        node = wrap_in_group(node)
    set_target_vals(node, source, source_vals, target)


def delete_labels(node, source):
    '''
    parameters: selected object, source
    Deletes source attribute values or node content
    returns: ?
    '''
    del_source_labels(node, source)


def move_labels(node, source, target, wrap=False):
    '''
    parameters: selected object, source and target option, wrap option
    Copies source attribute values or node content to target, deletes from source
    returns: ?
    '''
    copy_labels(node, source, target, wrap)
    delete_labels(node, source)


# ------------------------------------------------------------------- #
# functions: styles
# ------------------------------------------------------------------- #

def copy_style(scopes, source, target):
    '''
    parameters: scopes, source, target
    Copies style attributes from source to target
    returns: ?
    '''
    if source == "selection":
        style = util.get_style(scopes[source].values()[0])
    elif source == "string":
        style = scopes[source]
    else:
        style = util.get_style(scopes[source])
    if target == "selection":
        for node in scopes[target].itervalues():
            util.set_style(node, style)
    elif target == "groups":
        for node in util.get_groups(scopes['source_layer']):
            util.set_style(node, style)
    elif target != source:
        util.set_style(scopes[target], style)


def delete_style(scopes, source):
    '''
    parameters: scopes, source, target
    Deletes style attributes of scopes[source]
    returns: ?
    '''
    if source == "selection":
        for node in scopes[source].itervalues():
            util.del_style(node)
    elif source != "string":
        util.del_style(scopes[source])


def move_style(scopes, source, target):
    '''
    parameters: scopes, source, target
    Copy style attributes from source to target, delete style attribute from source
    returns: ?
    '''
    copy_style(scopes, source, target)
    delete_style(scopes, source)


# ------------------------------------------------------------------- #
# functions: transforms
# ------------------------------------------------------------------- #

def copy_transform(scopes, source, target):
    '''
    parameters: scopes, source, target
    Copies transform attributes from source to target
    returns: ?
    '''
    if source == "selection":
        transf = util.get_transform(scopes[source].values()[0])
    elif source == "string":
        transf = scopes[source]
    else:
        transf = util.get_transform(scopes[source])
    if target == "selection":
        for node in scopes[target].itervalues():
            util.set_transform(node, transf)
    elif target == "groups":
        for node in util.get_groups(scopes['source_layer']):
            util.set_transform(node, transf)
    elif target == "uses":
        for node in util.get_uses(scopes['source_layer']):
            util.set_transform(node, transf)
    elif target != source:
        util.set_transform(scopes[target], transf)


def delete_transform(scopes, source):
    '''
    parameters: scopes, source, target
    Deletes transform attributes of scopes[source]
    returns: ?
    '''
    if source == "selection":
        for node in scopes[source].itervalues():
            util.del_transform(node)
    elif source != "string":
        util.del_transform(scopes[source])


def move_transform(scopes, source, target):
    '''
    parameters: scopes, source, target
    Copy transform attributes from source to target, delete style attribute from source
    returns: ?
    '''
    copy_transform(scopes, source, target)
    delete_transform(scopes, source)


def translate_to_origin(scopes, source, target):
    '''
    parameters: scopes, source, target
    translates objects in source to SVG origin (0,0)
    returns: ?
    '''
    if source == 'selection':
        for node in scopes[source].itervalues():
            if util.is_group(node) and target == 'groups':
                # FIXME: only delete 'translate()' component (?)
                util.del_transform(node)
                xmin, ymin = list(util.get_approx_bbox([node]))[:2]
                for item in node.iterchildren():
                    if scopes['apply']:
                        util.apply_translate(item, xmin, ymin)
                    else:
                        util.add_translate(item, xmin, ymin)
            elif target in ('selection', 'groups'):
                xmin, ymin = list(util.get_approx_bbox([node]))[:2]
                if scopes['apply']:
                    util.apply_translate(node, xmin, ymin)
                else:
                    util.add_translate(node, xmin, ymin)


# ------------------------------------------------------------------- #
# functions: layer styles
# ------------------------------------------------------------------- #

# ------------------------------------------------------------------- #
# functions: svgfonts
# ------------------------------------------------------------------- #

def glyph_to_group(scopes, svgfont, glyph, glyphs_dict, transf):
    '''
    parameters: scopes, svgfont, glyph, glyph name mapping, transforms
    converts a glyph to a group
    returns: group element
    '''
    unicode_char = glyph.get("unicode")
    if unicode_char is not None:
        # FIXME: handle child elements of <glyph>
        glyph_group = path_to_group(glyph, scopes['target_layer'])
        if glyph_group is not None:
            if scopes['draw_em_rect'] or scopes['draw_baseline']:
                glyph_group.insert(0, add_glyph_em_ref(scopes, svgfont, glyph))
            glyph_group = transform_glyph_group(scopes, glyph_group, transf)
            glyph_info = get_glyph_info(svgfont, glyphs_dict, unicode_char)
            util.set_id(glyph_group, glyph_info['id'])
            if scopes['use_glyphname'] and 'glyph-name' in glyph.attrib:
                util.set_title_text(glyph_group, glyph.get('glyph-name'))
            else:
                util.set_title_text(glyph_group, glyph_info['name'])
            return glyph_group


def convert_svgfont_to_groups(scopes, defs):
    '''
    parameters: scopes, defs
    searches for all <glyphs> in first svg font found in defs and
    converts each to group (inverting the coords, scaling to useful size)
    returns: ?
    '''
    group_list = []
    svgfont = get_svgfont_info(scopes, defs)
    glyphs = svgfont['font'].findall(inkex.addNS('glyph', 'svg'))
    if glyphs:
        fit_node_to_area(scopes['root'], (svgfont['advance_x'], svgfont['emsize']))
        transf = get_svgfont_transforms(svgfont)
        glyphs_dict = get_glyph_mapping(scopes)
        for glyph in glyphs:
            glyph_group = glyph_to_group(scopes, svgfont, glyph, glyphs_dict, transf)
            if glyph_group is not None:
                group_list.append(glyph_group)
        if not scopes['keep_font']:
            if len(group_list):
                svgfont['font'].getparent().remove(svgfont['font'])
        if not scopes['title']:
            title = util.get_title_text(scopes['root'])
            if title is None:
                title = util.get_id(svgfont['font'])
            scopes['title'] = title
        util.set_title(scopes['root'], scopes['title'])
        if scopes['style']:
            util.set_style(scopes['root'], scopes['style'])


# ------------------------------------------------------------------- #
# functions: dispatcher
# ------------------------------------------------------------------- #

def convert_arrange_items(scopes, layout):
    '''
    parameters: scopes, layout
    dispatcher for converting and arranging of items
    returns: area (width/height) of layout, or None
    '''
    if scopes['action'] == 'clone':
        clone_items(scopes, layout, scopes['defs'])
    elif scopes['action'] == 'convert':
        convert_items(scopes, layout, scopes['defs'])
    elif scopes['action'] == 'arrange':
        arrange_items(scopes, layout, scopes['defs'])
    if layout['fit_page']:
        if layout['table_area'] is not None:
            fit_node_to_area(scopes['root'], layout['table_area'])


def import_items(scopes):
    '''
    parameters: scopes
    dispatcher for import of SVG files as groups
    returns: ?
    '''
    if scopes['action'] == 'import':
        import_as_groups(scopes)


def export_items(scopes):
    '''
    parameters: scopes
    dispatcher for import of SVG files as groups
    returns: ?
    '''
    if scopes['action'] == 'export':
        export_to_files(scopes)
    elif scopes['action'] == 'playground':
        try:
            playground.test_me(scopes)
        except NameError as e:
            inkex.debug('Playground option not available.\n{}'.format(e.value))


def label_items(scopes):
    '''
    parameters: scopes
    dispatcher for label actions per selected object
    returns: ?
    '''
    for node in scopes['selection'].itervalues():
        if scopes['action'] == "copy":
            copy_labels(node, scopes['source'], scopes['target'], scopes['wrap'])
        elif scopes['action'] == "move":
            move_labels(node, scopes['source'], scopes['target'], scopes['wrap'])
        elif scopes['action'] == "delete":
            delete_labels(node, scopes['source'])


def style_items(scopes):
    '''
    parameters: scopes
    dispatcher for style actions
    returns: ?
    '''
    if scopes['action'] == "copy":
        copy_style(scopes, scopes['source'], scopes['target'])
    elif scopes['action'] == "move":
        move_style(scopes, scopes['source'], scopes['target'])
    elif scopes['action'] == "delete":
        delete_style(scopes, scopes['source'])


def transform_items(scopes):
    '''
    parameters: scopes
    dispatcher for transform actions (fourth tab)
    returns: ?
    '''
    if scopes['action'] == "copy":
        copy_transform(scopes, scopes['source'], scopes['target'])
    elif scopes['action'] == "move":
        move_transform(scopes, scopes['source'], scopes['target'])
    elif scopes['action'] == "delete":
        delete_transform(scopes, scopes['source'])
    elif scopes['action'] == "origin":
        translate_to_origin(scopes, scopes['source'], scopes['target'])


def layer_items(scopes):
    '''
    parameters: scopes
    dispatcher for layer styles
    returns: ?
    '''
    # try:
    #     playground.test_me(scopes)
    # except NameError:
    #     inkex.debug('Not yet implemented for layer styles: {}'.format(scopes['action']))
    playground.test_me(scopes)


def use_items(scopes):
    '''
    parameters: scopes, defs
    dispatcher for functions acting on <symbol> definitions
    returns: ?
    '''
    if scopes['action'] == 'clone':
        clone_items(scopes, None, scopes['defs'])
    elif scopes['action'] == 'export':
        export_use_items(scopes, scopes['defs'])
    elif scopes['action'] == 'cleanup':
        cleanup_items(scopes, scopes['defs'])


def svgfonts_items(scopes):
    '''
    parameters: scopes
    dispatcher for functions acting on first SVG font in current document
    returns: ?
    '''
    if scopes['action'] == 'convert':
        convert_svgfont_to_groups(scopes, scopes['defs'])


# ------------------------------------------------------------------- #
# main class for effect
# ------------------------------------------------------------------- #

class SymbolTools(inkex.Effect):
    '''
    Tools to manipulate symbol and icon sets
    '''
    def __init__(self):
        inkex.Effect.__init__(self)
        # objects
        self.OptionParser.add_option("--convert_arrange_action",
                                     action="store", type="string",
                                     dest="convert_arrange_action", default="convert",
                                     help="")
        self.OptionParser.add_option("--convert_arrange_source",
                                     action="store", type="string",
                                     dest="convert_arrange_source", default="uses",
                                     help="")
        self.OptionParser.add_option("--fixed_id",
                                     action="store", type="inkbool",
                                     dest="fixed_id", default=False,
                                     help="")
        self.OptionParser.add_option("--layout_type",
                                     action="store", type="string",
                                     dest="layout_type", default="keep",
                                     help="")
        self.OptionParser.add_option("--offset_x",
                                     action="store", type="int",
                                     dest="offset_x", default=0,
                                     help="")
        self.OptionParser.add_option("--offset_y",
                                     action="store", type="int",
                                     dest="offset_y", default=0,
                                     help="")
        self.OptionParser.add_option("--spacing_x",
                                     action="store", type="int",
                                     dest="spacing_x", default=20,
                                     help="")
        self.OptionParser.add_option("--spacing_y",
                                     action="store", type="int",
                                     dest="spacing_y", default=20,
                                     help="")
        self.OptionParser.add_option("--per_column",
                                     action="store", type="int",
                                     dest="per_column", default=0,
                                     help="")
        self.OptionParser.add_option("--per_row",
                                     action="store", type="int",
                                     dest="per_row", default=0,
                                     help="")
        self.OptionParser.add_option("--width_factor",
                                     action="store", type="int",
                                     dest="width_factor", default=8,
                                     help="")
        self.OptionParser.add_option("--direction",
                                     action="store", type="string",
                                     dest="direction", default="horizontal",
                                     help="")
        self.OptionParser.add_option("--fit_page",
                                     action="store", type="inkbool",
                                     dest="fit_page", default=False,
                                     help="")
        # import
        self.OptionParser.add_option("--import_action",
                                     action="store", type="string",
                                     dest="import_action", default="import",
                                     help="")
        self.OptionParser.add_option("--import_source",
                                     action="store", type="string",
                                     dest="import_source", default="string",
                                     help="")
        self.OptionParser.add_option("--import_target",
                                     action="store", type="string",
                                     dest="import_target", default="groups",
                                     help="")
        self.OptionParser.add_option("--import_path",
                                     action="store", type="string",
                                     dest="import_path", default="",
                                     help="")
        self.OptionParser.add_option("--import_dir_walk",
                                     action="store", type="inkbool",
                                     dest="import_dir_walk", default=True,
                                     help="")
        self.OptionParser.add_option("--import_ignore_symlinks",
                                     action="store", type="inkbool",
                                     dest="import_ignore_symlinks", default=True,
                                     help="")
        self.OptionParser.add_option("--import_sort",
                                     action="store", type="string",
                                     dest="import_sort", default="dirname",
                                     help="")
        self.OptionParser.add_option("--import_clean_styles",
                                     action="store", type="inkbool",
                                     dest="import_clean_styles", default=True,
                                     help="")
        self.OptionParser.add_option("--import_viewport_rect",
                                     action="store", type="inkbool",
                                     dest="import_viewport_rect", default=True,
                                     help="")
        self.OptionParser.add_option("--import_title",
                                     action="store", type="string",
                                     dest="import_title", default="",
                                     help="")
        # export
        self.OptionParser.add_option("--export_action",
                                     action="store", type="string",
                                     dest="export_action", default="export",
                                     help="")
        self.OptionParser.add_option("--export_source",
                                     action="store", type="string",
                                     dest="export_source", default="groups",
                                     help="")
        self.OptionParser.add_option("--export_target",
                                     action="store", type="string",
                                     dest="export_target", default="string",
                                     help="")
        self.OptionParser.add_option("--export_path",
                                     action="store", type="string",
                                     dest="export_path", default="",
                                     help="")
        self.OptionParser.add_option("--export_page_size",
                                     action="store", type="string",
                                     dest="export_page_size", default="keep",
                                     help="")
        self.OptionParser.add_option("--export_page_position",
                                     action="store", type="string",
                                     dest="export_page_position", default="keep",
                                     help="")
        self.OptionParser.add_option("--export_apply_transforms",
                                     action="store", type="inkbool",
                                     dest="export_apply_transforms", default=False,
                                     help="")
        self.OptionParser.add_option("--export_basename",
                                     action="store", type="string",
                                     dest="export_basename", default="index",
                                     help="")
        self.OptionParser.add_option("--export_basename_string",
                                     action="store", type="string",
                                     dest="export_basename_string", default="sample",
                                     help="")
        self.OptionParser.add_option("--export_suffix",
                                     action="store", type="string",
                                     dest="export_suffix", default="",
                                     help="")
        self.OptionParser.add_option("--export_suffix_string",
                                     action="store", type="string",
                                     dest="export_suffix_string", default="",
                                     help="")
        self.OptionParser.add_option("--export_layer_context",
                                     action="store", type="inkbool",
                                     dest="export_layer_context", default=False,
                                     help="")
        # labels
        self.OptionParser.add_option("--labels_action",
                                     action="store", type="string",
                                     dest="labels_action", default="copy",
                                     help="")
        self.OptionParser.add_option("--labels_source",
                                     action="store", type="string",
                                     dest="labels_source", default="id",
                                     help="")
        self.OptionParser.add_option("--labels_target",
                                     action="store", type="string",
                                     dest="labels_target", default="id",
                                     help="")
        self.OptionParser.add_option("--reverse_copy",
                                     action="store", type="inkbool",
                                     dest="reverse_copy", default=False,
                                     help="")
        self.OptionParser.add_option("--prep_group",
                                     action="store", type="inkbool",
                                     dest="prep_group", default=False,
                                     help="")
        # styles
        self.OptionParser.add_option("--styles_action",
                                     action="store", type="string",
                                     dest="styles_action", default="copy",
                                     help="")
        self.OptionParser.add_option("--styles_source",
                                     action="store", type="string",
                                     dest="styles_source", default="selection",
                                     help="")
        self.OptionParser.add_option("--styles_target",
                                     action="store", type="string",
                                     dest="styles_target", default="selection",
                                     help="")
        self.OptionParser.add_option("--styles_style",
                                     action="store", type="string",
                                     dest="styles_style", default="stroke:none",
                                     help="")
        # transforms
        self.OptionParser.add_option("--transforms_action",
                                     action="store", type="string",
                                     dest="transforms_action", default="copy",
                                     help="")
        self.OptionParser.add_option("--transforms_source",
                                     action="store", type="string",
                                     dest="transforms_source", default="selection",
                                     help="")
        self.OptionParser.add_option("--transforms_target",
                                     action="store", type="string",
                                     dest="transforms_target", default="selection",
                                     help="")
        self.OptionParser.add_option("--transforms_transform",
                                     action="store", type="string",
                                     dest="transforms_transform", default="scale(1, -1)",
                                     help="")
        self.OptionParser.add_option("--transforms_apply",
                                     action="store", type="inkbool",
                                     dest="transforms_apply", default=False,
                                     help="")
        # layers
        self.OptionParser.add_option("--layers_action",
                                     action="store", type="string",
                                     dest="layers_action", default="copy",
                                     help="")
        self.OptionParser.add_option("--layers_test",
                                     action="store", type="string",
                                     dest="layers_test", default="",
                                     help="")
        # symbols
        self.OptionParser.add_option("--symbols_action",
                                     action="store", type="string",
                                     dest="symbols_action", default="clone",
                                     help="")
        self.OptionParser.add_option("--symbols_source",
                                     action="store", type="string",
                                     dest="symbols_source", default="symbol",
                                     help="")
        self.OptionParser.add_option("--symbols_export_path",
                                     action="store", type="string",
                                     dest="symbols_export_path", default="",
                                     help="")
        self.OptionParser.add_option("--symbols_export_basename",
                                     action="store", type="string",
                                     dest="symbols_export_basename", default="index",
                                     help="")
        # svgfonts
        self.OptionParser.add_option("--svgfonts_action",
                                     action="store", type="string",
                                     dest="svgfonts_action", default="convert",
                                     help="")
        self.OptionParser.add_option("--svgfonts_source",
                                     action="store", type="string",
                                     dest="svgfonts_source", default="svgfont",
                                     help="")
        self.OptionParser.add_option("--svgfonts_target",
                                     action="store", type="string",
                                     dest="svgfonts_target", default="groups",
                                     help="")
        self.OptionParser.add_option("--svgfonts_basetile",
                                     action="store", type="int",
                                     dest="svgfonts_basetile", default=48,
                                     help="")
        self.OptionParser.add_option("--svgfonts_draw_em_rect",
                                     action="store", type="inkbool",
                                     dest="svgfonts_draw_em_rect", default=True,
                                     help="")
        self.OptionParser.add_option("--svgfonts_draw_baseline",
                                     action="store", type="inkbool",
                                     dest="svgfonts_draw_baseline", default=False,
                                     help="")
        self.OptionParser.add_option("--svgfonts_ignore_descent",
                                     action="store", type="inkbool",
                                     dest="svgfonts_ignore_descent", default=False,
                                     help="")
        self.OptionParser.add_option("--svgfonts_keep_font",
                                     action="store", type="inkbool",
                                     dest="svgfonts_keep_font", default=False,
                                     help="")
        self.OptionParser.add_option("--svgfonts_flatten_transforms",
                                     action="store", type="inkbool",
                                     dest="svgfonts_flatten_transforms", default=False,
                                     help="")
        self.OptionParser.add_option("--svgfonts_apply_transforms",
                                     action="store", type="inkbool",
                                     dest="svgfonts_apply_transforms", default=False,
                                     help="")
        # icon mapping
        self.OptionParser.add_option("--svgfonts_iconmap_glyphname",
                                     action="store", type="inkbool",
                                     dest="svgfonts_iconmap_glyphname", default=False,
                                     help="")
        self.OptionParser.add_option("--svgfonts_iconmap_nam",
                                     action="store", type="string",
                                     dest="svgfonts_iconmap_nam", default="",
                                     help="")
        self.OptionParser.add_option("--svgfonts_iconmap_yml",
                                     action="store", type="string",
                                     dest="svgfonts_iconmap_yml", default="",
                                     help="")
        self.OptionParser.add_option("--svgfonts_iconmap_json",
                                     action="store", type="string",
                                     dest="svgfonts_iconmap_json", default="",
                                     help="")
        self.OptionParser.add_option("--svgfonts_iconmap_css",
                                     action="store", type="string",
                                     dest="svgfonts_iconmap_css", default="",
                                     help="")
        self.OptionParser.add_option("--svgfonts_iconmap_csv",
                                     action="store", type="string",
                                     dest="svgfonts_iconmap_csv", default="",
                                     help="")
        self.OptionParser.add_option("--svgfonts_iconmap_dict",
                                     action="store", type="string",
                                     dest="svgfonts_iconmap_dict", default="",
                                     help="")
        self.OptionParser.add_option("--svgfonts_iconmap_presets",
                                     action="store", type="string",
                                     dest="svgfonts_iconmap_presets", default="",
                                     help="")
        self.OptionParser.add_option("--svgfonts_title",
                                     action="store", type="string",
                                     dest="svgfonts_title", default="",
                                     help="")
        self.OptionParser.add_option("--svgfonts_style",
                                     action="store", type="string",
                                     dest="svgfonts_style", default="stroke:none",
                                     help="")
        # tabs
        self.OptionParser.add_option("--nb_main",
                                     action="store", type="string",
                                     dest="nb_main")
        self.OptionParser.add_option("--nb_svgfonts_config",
                                     action="store", type="string",
                                     dest="nb_svgfonts_config")

    def page_objects(self):
        ''' Applies effect to objects '''
        scopes = {
            'action': self.options.convert_arrange_action,
            'source': self.options.convert_arrange_source,
            'defs': self.xpathSingle('/svg:svg//svg:defs'),
            'selection': self.selected,
            'fixed_id': self.options.fixed_id,
            'source_layer': self.current_layer,
            'target_layer': self.current_layer,
            'root': self.document.getroot()
        }
        layout = {
            'type': self.options.layout_type,
            'offset_x': self.options.offset_x,
            'offset_y': self.options.offset_y,
            'cell_width': self.options.spacing_x,
            'cell_height': self.options.spacing_y,
            'per_row': self.options.per_row,
            'per_column': self.options.per_column,
            'width_factor': self.options.width_factor,
            'direction': self.options.direction,
            'fit_page': self.options.fit_page,
            'table_area': None
        }
        convert_arrange_items(scopes, layout)

    def page_import(self):
        ''' Applies effect to import SVG files as groups'''
        scopes = {
            'action': self.options.import_action,
            'source': self.options.import_source,
            'target': self.options.import_target,
            'target_layer': self.current_layer,
            'root': self.document.getroot(),
            'path': self.options.import_path,
            'dir_walk': self.options.import_dir_walk,
            'ignore_symlinks': self.options.import_ignore_symlinks,
            'sort_by': self.options.import_sort,
            'clean_styles': self.options.import_clean_styles,
            'viewport_rect': self.options.import_viewport_rect,
            'title': self.options.import_title,
            'dc_creator': None,
            'dc_publisher': None,
            'dc_source': None,
            'dc_desc': None,
            'import_defs': False,
        }
        import_items(scopes)

    def page_export(self):
        ''' Applies effect to export source to SVG files'''
        scopes = {
            'action': self.options.export_action,
            'source': self.options.export_source,
            'target': self.options.export_target,
            'root': self.document.getroot(),
            'source_layer': self.current_layer,
            'defs': self.xpathSingle('/svg:svg//svg:defs'),
            'selection': self.selected,
            'path': self.options.export_path,
            'page_size': self.options.export_page_size,
            'position': self.options.export_page_position,
            'basename': self.options.export_basename,
            'basename_string': self.options.export_basename_string,
            'suffix': self.options.export_suffix,
            'suffix_string': self.options.export_suffix_string,
            'layer_context': self.options.export_layer_context,
            'svg_file': self.svg_file,
        }
        export_items(scopes)

    def page_labels(self):
        ''' Applies effect to labels '''
        scopes = {
            'action': self.options.labels_action,
            'source': self.options.labels_source,
            'target': self.options.labels_target,
            'selection': self.selected,
            'wrap': self.options.prep_group
        }
        label_items(scopes)

    def page_styles(self):
        ''' Applies effect to styles '''
        scopes = {
            'action': self.options.styles_action,
            'source': self.options.styles_source,
            'target': self.options.styles_target,
            'selection': self.selected,
            'source_layer': self.current_layer,
            'target_layer': self.current_layer,
            'root': self.document.getroot(),
            'layer': self.current_layer,
            'string': self.options.styles_style
        }
        style_items(scopes)

    def page_transforms(self):
        ''' Applies effect to styles '''
        scopes = {
            'action': self.options.transforms_action,
            'source': self.options.transforms_source,
            'target': self.options.transforms_target,
            'selection': self.selected,
            'source_layer': self.current_layer,
            'target_layer': self.current_layer,
            'root': self.document.getroot(),
            'layer': self.current_layer,
            'string': self.options.transforms_transform,
            'apply': self.options.transforms_apply,
        }
        transform_items(scopes)

    def page_layers(self):
        ''' Applies effect to layers '''
        scopes = {
            'action': self.options.layers_action,
            'root': self.document.getroot(),
            'test': self.options.layers_test,
            'fill_color': "#bebebe",
            'stroke_color': "#bebebe",
            'source_layer': self.current_layer,
            'target_layer': self.current_layer
        }
        layer_items(scopes)

    def page_symbols(self):
        ''' Applies effect to symbols '''
        scopes = {
            'action': self.options.symbols_action,
            'source': self.options.symbols_source,
            'root': self.document.getroot(),
            'defs': self.xpathSingle('/svg:svg//svg:defs'),
            'target_layer': self.current_layer,
            'path': self.options.symbols_export_path,
            'basename': self.options.symbols_export_basename,
        }
        use_items(scopes)

    def page_svgfonts(self):
        ''' Applies effect to svgfonts '''
        scopes = {
            'action': self.options.svgfonts_action,
            'source': self.options.svgfonts_source,
            'target': self.options.svgfonts_target,
            'defs': self.xpathSingle('/svg:svg//svg:defs'),
            'target_layer': self.current_layer,
            'root': self.document.getroot(),
            'basetile': self.options.svgfonts_basetile,
            'keep_font': self.options.svgfonts_keep_font,
            'draw_em_rect': self.options.svgfonts_draw_em_rect,
            'draw_baseline': self.options.svgfonts_draw_baseline,
            'ignore_descent': self.options.svgfonts_ignore_descent,
            'flatten_transforms': self.options.svgfonts_flatten_transforms,
            'apply_transforms': self.options.svgfonts_apply_transforms,
            'glyphname_as_title': self.options.svgfonts_iconmap_glyphname,
            'use_glyphname': False,
            'title': self.options.svgfonts_title,
            'style': self.options.svgfonts_style
        }
        if self.options.nb_svgfonts_config == '"page_svgfonts_config_glyphname"':
            scopes['mapping'] = ['glyphname', self.options.svgfonts_iconmap_glyphname]
        elif self.options.nb_svgfonts_config == '"page_svgfonts_config_nam"':
            scopes['mapping'] = ['namelist', self.options.svgfonts_iconmap_nam]
        elif self.options.nb_svgfonts_config == '"page_svgfonts_config_yml"':
            scopes['mapping'] = ['yaml', self.options.svgfonts_iconmap_yml]
        elif self.options.nb_svgfonts_config == '"page_svgfonts_config_json"':
            scopes['mapping'] = ['json', self.options.svgfonts_iconmap_json]
        elif self.options.nb_svgfonts_config == '"page_svgfonts_config_css"':
            scopes['mapping'] = ['css', self.options.svgfonts_iconmap_css]
        elif self.options.nb_svgfonts_config == '"page_svgfonts_config_csv"':
            scopes['mapping'] = ['csv', self.options.svgfonts_iconmap_csv]
        elif self.options.nb_svgfonts_config == '"page_svgfonts_config_dict"':
            scopes['mapping'] = ['dict', self.options.svgfonts_iconmap_dict]
        elif self.options.nb_svgfonts_config == '"page_svgfonts_config_presets"':
            scopes['mapping'] = ['presets', self.options.svgfonts_iconmap_presets]
        svgfonts_items(scopes)

    def effect(self):
        ''' Converts symbols to groups or groups to symbols, arranges them in grid '''
        try:
            page_cmd = getattr(self, self.options.nb_main.strip("\""))
        except AttributeError:
            page_cmd = None
            inkex.errormsg('Unknown tab {}'.format(self.options.nb_main))
        if page_cmd is not None:
            page_cmd()


if __name__ == '__main__':
    e = SymbolTools()
    e.affect()

# vim: expandtab shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=99
