#!/usr/bin/env python
'''
css_util.py - read icon name/unicode code mapping from CSS file

Copyright (C) 2015, ~suv <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
'''
# pylint: disable=invalid-name
# pylint: disable=missing-docstring

# system modules
import os
import tinycss
# internal modules
import inkex

# ------------------------------------------------------------------- #
# globals
# ------------------------------------------------------------------- #


# ------------------------------------------------------------------- #
# utility functions for extracting icon mapping from JSON files
# ------------------------------------------------------------------- #

def parse_font_stylesheet(config_file):
    '''
    parameters: config file (full path)
    parses config file and creates dict using unicode as key, id and name as value
    returns: dict

    Icon fonts known to provide this format:
    * Octicons
    * Foundation Icons
    * open-iconic
    * Ionicons

    '''
    stylesheet = None
    adict = {}
    parser = tinycss.make_parser('page3')
    try:
        with open(config_file, 'r') as f:
            stylesheet = parser.parse_stylesheet_file(f)
    except Exception:
        pass
    if stylesheet is not None:
        # TODO: improve usage of tinycss (or use different CSS parser module)
        for rule in stylesheet.rules:
            for decl in rule.declarations:
                if str(decl.name) == 'content':
                    # FIXME: improve string extraction
                    decl_val = str(decl.value.as_css())
                    decl_val = decl_val.strip('"')
                    decl_val = decl_val.strip("'")
                    unicode_str = decl_val[1:]
                    icon_name = str(rule.selector.as_css()).split(':')[0]
                    adict[unicode_str] = {'id': unicode_str, 'name': icon_name}
        inkex.debug(adict)
        return adict


def parse_config_file(config_file):
    '''
    parameters: config file (full path)
    dispatcher for known formats based on file name (needs fixing)
    returns: dict
    '''
    if os.path.isfile(config_file):
        # TODO: check for possible variants which need different parsing
        # filename = os.path.split(config_file)[1]
        # if filename == '{}.css'.format(fontname):
        #     return parse_font_stylesheet(config_file)
        return parse_font_stylesheet(config_file)


# vim: expandtab shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=99
